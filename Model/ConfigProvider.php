<?php

/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Mapi\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Resursbank\Core\Api\Data\PaymentMethodInterface;
use Resursbank\Core\Helper\PaymentMethods as CorePaymentMethods;
use Resursbank\Core\Helper\PaymentMethods\Ecom as PaymentMethods;
use Resursbank\Ecom\Lib\Model\PaymentMethod;
use Resursbank\Ecom\Lib\Model\PriceSignage\PriceSignage;
use Resursbank\Ecom\Module\PaymentMethod\Repository;
use Resursbank\Ecom\Module\PaymentMethod\Repository as PaymentMethodRepository;
use Resursbank\Ecom\Module\PriceSignage\Widget\CostList;
use Resursbank\Ecom\Module\PaymentMethod\Widget\ReadMore;
use Resursbank\Ecom\Module\PriceSignage\Widget\Warning;
use Resursbank\Mapi\Helper\Log;
use Resursbank\Core\ViewModel\Session\Checkout as CheckoutSession;
use Throwable;
use \Resursbank\Ecom\Module\PriceSignage\Repository as GetPriceSignageRepository;

/**
 * Gather all of our payment methods and put them in their own section of the
 * "checkoutConfig" object on the checkout page.
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.CookieAndSessionMisuse)
 */
class ConfigProvider implements ConfigProviderInterface
{
    /**
     * @param Log $log
     * @param PaymentMethods $helper
     * @param CheckoutSession $session
     * @param CorePaymentMethods $corePaymentMethods
     */
    public function __construct(
        private readonly Log $log,
        private readonly PaymentMethods $helper,
        private readonly CheckoutSession $session,
        private readonly CorePaymentMethods $corePaymentMethods
    ) {
    }

    /**
     * Builds this module's section in the config provider.
     *
     * @return array<string, mixed>
     */
    public function getConfig(): array
    {
        $result = [
            'payment' => [
                'resursbank_mapi' => [
                    'methods' => []
                ]
            ]
        ];

        try {
            $methods = $this->helper->getMethods(
                scopeCode: $this->session->getQuote()->getStore()->getCode()
            );

            foreach ($methods as $method) {
                $result['payment']['resursbank_mapi']['methods'][] =
                    $this->mapPaymentMethod(method: $method);
            }
        } catch (Throwable $error) {
            $this->log->exception(error: $error);
        }

        return $result;
    }

    /**
     * Maps selective data from payment method to the config provider.
     *
     * @param PaymentMethodInterface $method
     * @return array<string, mixed>
     */
    private function mapPaymentMethod(
        PaymentMethodInterface $method
    ): array {
        $resursBankMethod = $this->getMethod(method: $method);

        $result = [
            'code' => $method->getCode(),
            'title' => $method->getTitle(),
            'maxOrderTotal' => $method->getMaxOrderTotal(),
            'minOrderTotal' => $method->getMinOrderTotal(),
            'sortOrder' => $method->getSortOrder(default: 0),
            'type' => $method->getType(),
            'specificType' => $method->getSpecificType(),
            'customerType' => $this->corePaymentMethods->getCustomerTypes(
                method: $method
            ),
            'usp' => '',
            'readMore' => '',
            'costList' => '',
            'priceSignageWarning' => ''
        ];

        if ($resursBankMethod !== null) {
            $result['usp'] = $this->getUspMessage(method: $resursBankMethod);
            $result['readMore'] = $this->getReadMore(method: $resursBankMethod);

            $priceSignage = $this->getPriceSignage(method: $resursBankMethod);

            if ($priceSignage !== null) {
                $result['costList'] = $this->getCostList(
                    method: $resursBankMethod,
                    priceSignage: $priceSignage
                );
                $result['priceSignageWarning'] = $this->getPriceSignageWarning(
                    priceSignage: $priceSignage,
                    paymentMethod: $resursBankMethod
                );
            }
        }

        return $result;
    }

    /**
     * Render read more widget for payment method and return HTML.
     *
     * @param PaymentMethod $method
     * @return string
     */
    private function getReadMore(PaymentMethod $method): string
    {
        try {
            /** @noinspection PhpCastIsUnnecessaryInspection */
            return (new ReadMore(
                paymentMethod: $method,
                amount: (float) $this->session->getQuote()->getGrandTotal(),
            ))->content;
        } catch (Throwable $error) {
            $this->log->exception(error: $error);
            return '';
        }
    }

    /**
     * Render Cost List widget and return HTML.
     *
     * @param PaymentMethod $method
     * @return string
     */
    private function getCostList(
        PaymentMethod $method,
        PriceSignage $priceSignage
    ): string {
        try {
            /** @noinspection PhpCastIsUnnecessaryInspection */
            return (new CostList(priceSignage: $priceSignage, method: $method))->content;
        } catch (Throwable $error) {
            $this->log->exception(error: $error);
            return '';
        }
    }

    /**
     * Render Price Signage Warning widget HTML.
     *
     * @return string
     */
    private function getPriceSignageWarning(
        PriceSignage $priceSignage,
        PaymentMethod $paymentMethod
    ): string {
        try {
            /** @noinspection PhpCastIsUnnecessaryInspection */
            return (new Warning(
                priceSignage: $priceSignage,
                paymentMethod: $paymentMethod
            ))->content;
        } catch (Throwable $error) {
            $this->log->exception(error: $error);
            return '';
        }
    }

    /**
     * Fetches the USP message for specified method code.
     *
     * @param PaymentMethod $method
     * @return string
     */
    private function getUspMessage(PaymentMethod $method): string
    {
        try {
            /** @noinspection PhpCastIsUnnecessaryInspection */
            return PaymentMethodRepository::getUniqueSellingPoint(
                paymentMethod: $method,
                amount: (float) $this->session->getQuote()->getGrandTotal()
            )->message;
        } catch (Throwable) {
            return '';
        }
    }

    /**
     * Get payment method from Resurs Bank API using the method code.
     *
     * @param PaymentMethodInterface $method
     * @return PaymentMethod|null
     */
    private function getMethod(PaymentMethodInterface $method): ?PaymentMethod
    {
        try {
            return Repository::getById(
                paymentMethodId: $this->helper->getUuidFromCode($method->getCode())
            );
        } catch (Throwable $error) {
            $this->log->exception(error: $error);
            return null;
        }
    }

    /**
     * Get price signage for specified payment method.
     *
     * @param PaymentMethod $method
     * @return PriceSignage|null
     */
    private function getPriceSignage(PaymentMethod $method): ?PriceSignage
    {
        if ($method->isInternal()) {
            try {
                return GetPriceSignageRepository::getPriceSignage(
                    paymentMethodId: $method->id,
                    amount: (float)$this->session->getQuote()->getGrandTotal()
                );
            } catch (Throwable $error) {
                $this->log->exception(error: $error);
                return null;
            }
        }

        return null;
    }
}
