<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */
declare(strict_types=1);

namespace Resursbank\Mapi\Test\Integration;

use Magento\Sales\Api\Data\OrderExtensionInterface;
use Magento\GiftMessage\Api\Data\MessageInterface;

/**
 * Dummy data object to be used by OrderLineTest.
 */
class ExtensionAttributesObject implements OrderExtensionInterface
{
    /** @var mixed */
    private mixed $appliedTaxes;

    /** @var mixed  */
    private mixed $itemAppliedTaxes;

    /** @var mixed */
    private mixed $convertingFromQuote;

    public function setAppliedTaxes($appliedTaxes): void
    {
        $this->appliedTaxes = $appliedTaxes;
    }

    public function getAppliedTaxes(): mixed
    {
        return $this->appliedTaxes;
    }

    public function setItemAppliedTaxes($itemAppliedTaxes): void
    {
        $this->itemAppliedTaxes = $itemAppliedTaxes;
    }

    public function getItemAppliedTaxes(): mixed
    {
        return $this->itemAppliedTaxes;
    }

    public function setConvertingFromQuote($convertingFromQuote): void
    {
        $this->convertingFromQuote = $convertingFromQuote;
    }

    public function getConvertingFromQuote(): mixed
    {
        return $this->convertingFromQuote;
    }

    public function getShippingAssignments()
    {
        // stub
    }

    /**
     * @param $shippingAssignments
     * @return void
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function setShippingAssignments($shippingAssignments)
    {
        // stub
    }

    /**
     * @return void
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function getPaymentAdditionalInfo()
    {
        // stub
    }

    /**
     * @param $paymentAdditionalInfo
     * @return void
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function setPaymentAdditionalInfo($paymentAdditionalInfo)
    {
        // stub
    }

    /**
     * @return void
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function getTaxes()
    {
        // stub
    }

    /**
     * @param $taxes
     * @return void
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function setTaxes($taxes)
    {
        // stub
    }

    /**
     * @return void
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function getAdditionalItemizedTaxes()
    {
        // stub
    }

    /**
     * @param $additionalItemizedTaxes
     * @return void
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function setAdditionalItemizedTaxes($additionalItemizedTaxes)
    {
        // stub
    }

    /**
     * @return void
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function getGiftMessage()
    {
        // stub
    }

    /**
     * @param MessageInterface $giftMessage
     * @return void
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function setGiftMessage(MessageInterface $giftMessage)
    {
        // stub
    }

    /**
     * @return void
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function getPickupLocationCode()
    {
        // stub
    }

    /**
     * @param $pickupLocationCode
     * @return void
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function setPickupLocationCode($pickupLocationCode)
    {
        // stub
    }

    /**
     * @return void
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function getNotificationSent()
    {
        // stub
    }

    /**
     * @param $notificationSent
     * @return void
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function setNotificationSent($notificationSent)
    {
        // stub
    }

    /**
     * @return void
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function getSendNotification()
    {
        // stub
    }

    /**
     * @param $sendNotification
     * @return void
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function setSendNotification($sendNotification)
    {
        // stub
    }
}
