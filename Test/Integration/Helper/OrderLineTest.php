<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Mapi\Test\Integration\Helper;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Model\Order\Item;
use Magento\Sales\Model\ResourceModel\Order\Creditmemo\Item\Collection;
use PHPUnit\Framework\TestCase;
use JsonException;
use Magento\TestFramework\Helper\Bootstrap;
use Magento\Sales\Model\Order\Creditmemo;
use Magento\Sales\Model\Order\Invoice;
use Magento\Framework\ObjectManagerInterface;
use ReflectionException;
use Resursbank\Ecom\Exception\AttributeCombinationException;
use Resursbank\Ecom\Exception\Validation\IllegalTypeException;
use Resursbank\Mapi\Helper\OrderLine;
use Resursbank\Ordermanagement\Model\Api\Payment\Converter\InvoiceConverter;
use Resursbank\Ordermanagement\Model\Api\Payment\Converter\CreditmemoConverter;
use Resursbank\Ordermanagement\Model\Api\Payment\Converter\OrderConverter;
use Magento\Sales\Api\CreditmemoRepositoryInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Resursbank\Ecom\Lib\Model\Payment\Order\ActionLog\OrderLine
    as EcomOrderLine;
use Magento\Tax\Api\OrderTaxManagementInterface;
use Resursbank\Mapi\Test\Integration\ExtensionAttributesObject;
use Resursbank\Core\Model\Api\Payment\Item as PaymentItem;

/**
 * @magentoAppArea frontend
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class OrderLineTest extends TestCase
{
    /** @var ObjectManagerInterface */
    private $objectManager;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        $this->objectManager = Bootstrap::getObjectManager();
    }

    /**
     * Verify convertInvoice output.
     *
     * @return void
     * @throws AttributeCombinationException
     * @throws IllegalTypeException
     * @throws JsonException
     * @throws ReflectionException
     * @magentoDataFixture Magento/Sales/_files/invoice.php
     */
    public function testConvertInvoice(): void
    {
        $invoice = $this->getInvoice();
        $item = $invoice->getItems()[3];
        $orderLineHelper = $this->getOrderLineHelper();
        $converted = $orderLineHelper->convertInvoice(invoice: $invoice);

        static::assertCount(
            expectedCount: count($invoice->getItems()),
            haystack: $converted
        );

        /** @var EcomOrderLine $convertedItem */
        $convertedItem = $converted[0];

        $properties = [
            'quantity' => 'qty',
            'reference' => 'sku',
            'description' => 'name'
        ];

        foreach ($properties as $convertedProperty => $originalProperty) {
            static::assertEquals(
                expected: $item->getData(key: $originalProperty),
                actual: $convertedItem->$convertedProperty
            );
        }
    }

    /**
     * Verify convertCreditmemo output.
     *
     * @return void
     * @throws AttributeCombinationException
     * @throws IllegalTypeException
     * @throws JsonException
     * @throws ReflectionException
     * @magentoDataFixture Magento/Sales/_files/order_with_invoice_shipment_creditmemo.php
     */
    public function testConvertCreditmemo(): void
    {
        $creditmemo = $this->getCreditmemo();

        /** @var Collection $foo */
        $items = [$creditmemo->getItemsCollection()->getItemById(idValue: 2)];
        $creditmemo->setItems(items: $items);
        $item = $creditmemo->getItems()[0];

        $orderLineHelper = $this->getOrderLineHelper();
        $converted = $orderLineHelper->convertCreditmemo(
            creditmemo: $creditmemo
        );

        /** @var EcomOrderLine $convertedItem */
        $convertedItem = $converted[0];

        $properties = [
            'quantity' => 'qty',
            'reference' => 'sku',
            'description' => 'name'
        ];

        foreach ($properties as $convertedProperty => $originalProperty) {
            static::assertEquals(
                expected: $item->getData(key: $originalProperty),
                actual: $convertedItem->$convertedProperty
            );
        }
    }

    /**
     * Verify convertOrder output.
     *
     * @return void
     * @throws AttributeCombinationException
     * @throws IllegalTypeException
     * @throws JsonException
     * @throws ReflectionException
     * @throws NoSuchEntityException
     * @magentoDataFixture Resursbank_Mapi::Test/Integration/_files/order.php
     */
    public function testConvertOrder(): void
    {
        $order = $this->getOrder();

        /** @var Item $item */
        $item = $order->getItems()[5];

        $orderLineHelper = $this->getOrderLineHelper();
        $converted = $orderLineHelper->convertOrder(order: $order);

        /** @var EcomOrderLine $convertedItem */
        $convertedItem = $converted[0];

        $properties = [
            'quantity' => 'qty_ordered',
            'vatRate' => 'tax_percent',
            'totalAmountIncludingVat' => 'row_total_incl_tax',
            'description' => 'name',
            'reference' => 'sku'
        ];

        static::assertEquals(
            expected: PaymentItem::UNIT_MEASURE,
            actual: $convertedItem->quantityUnit
        );
        foreach ($properties as $convertedProperty => $originalProperty) {
            static::assertEquals(
                expected: $item->getData(key: $originalProperty),
                actual: $convertedItem->$convertedProperty,
                message: 'Value of property ' . $convertedProperty .
                    ' does not match value of original property ' .
                    $originalProperty . '.'
            );
        }
    }

    /**
     * @return Creditmemo
     */
    private function getCreditmemo(): Creditmemo
    {
        /** @var CreditmemoRepositoryInterface $creditMemoRepository */
        $creditMemoRepository = $this->objectManager->get(
            type: CreditmemoRepositoryInterface::class
        );
        return $creditMemoRepository->get(id: 2);
    }

    /**
     * @return Invoice
     */
    private function getInvoice(): Invoice
    {
        return $this->objectManager->get(type: Invoice::class)
            ->loadByIncrementId('000000003');
    }

    /**
     * @return OrderInterface
     * @throws NoSuchEntityException
     */
    private function getOrder(): OrderInterface
    {
        $order = $this->objectManager->get(type: OrderInterface::class)
            ->loadByIncrementId('100000001');

        /** @var OrderTaxManagementInterface $orderTaxManagement */
        $orderTaxManagement = $this->objectManager->get(
            type: OrderTaxManagementInterface::class
        );
        $orderTaxDetails = $orderTaxManagement->getOrderTaxDetails(
            orderId: $order->getEntityId()
        );
        $appliedTaxes = $orderTaxDetails->getAppliedTaxes();

        /** @var ExtensionAttributesObject $extensionAttributes */
        $extensionAttributes = $this->objectManager->get(
            type: ExtensionAttributesObject::class
        );
        $extensionAttributes->setAppliedTaxes(appliedTaxes: $appliedTaxes);
        $extensionAttributes->setConvertingFromQuote(
            convertingFromQuote: false
        );
        $items = $order->getItems();
        $extensionAttributes->setItemAppliedTaxes(itemAppliedTaxes: $items);
        $order->setExtensionAttributes($extensionAttributes);

        return $order;
    }

    /**
     * Fetch an OrderLine helper object.
     *
     * @return OrderLine
     */
    private function getOrderLineHelper(): OrderLine
    {
        return new OrderLine(
            invoiceConverter: $this->objectManager->get(
                type: InvoiceConverter::class,
            ),
            creditmemoConverter: $this->objectManager->get(
                type: CreditmemoConverter::class,
            ),
            orderConverter: $this->objectManager->get(
                type: OrderConverter::class,
            )
        );
    }
}
