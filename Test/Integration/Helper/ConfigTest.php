<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Mapi\Test\Integration\Helper;

use Magento\Framework\ObjectManagerInterface;
use PHPUnit\Framework\TestCase;
use Magento\TestFramework\Helper\Bootstrap;
use Resursbank\Mapi\Helper\Config as ConfigHelper;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\App\Helper\Context;
use Resursbank\Core\Helper\Config as CoreConfig;
use Magento\Framework\Encryption\EncryptorInterface;

/**
 * @magentoAppArea frontend
 */
class ConfigTest extends TestCase
{
    /** @var ObjectManagerInterface  */
    private $objectManager;

    /**
     * @inheritDoc
     *
     * @return void
     */
    protected function setUp(): void
    {
        $this->objectManager = Bootstrap::getObjectManager();
    }

    /**
     * Verify that getClientId returns the correct config data.
     *
     * @return void
     * @magentoConfigFixture default_store resursbank/api/client_id_mapi_1 test_client_id
     */
    public function testGetClientId(): void
    {
        $config = $this->getConfigHelper();
        $clientId = $config->getClientId(
            scopeCode: 'default'
        );

        static::assertEquals(
            expected: 'test_client_id',
            actual: $clientId
        );
    }

    /**
     * Verify that getMagentoCronCleanupTtl returns the correct config data.
     *
     * @return void
     * @magentoConfigFixture default_store sales/orders/delete_pending_after 42
     */
    public function testGetMagentoCronCleanupTtl(): void
    {
        $config = $this->getConfigHelper();
        $clientId = $config->getMagentoCronCleanupTtl(
            scopeCode: 'default'
        );

        static::assertEquals(
            expected: '42',
            actual: $clientId
        );
    }

    /**
     * Fetch Config helper.
     *
     * @return ConfigHelper
     */
    private function getConfigHelper(): ConfigHelper
    {
        return new ConfigHelper(
            reader: $this->objectManager->get(
                type: ScopeConfigInterface::class
            ),
            writer: $this->objectManager->get(
                type: WriterInterface::class
            ),
            context: $this->objectManager->get(
                type: Context::class
            ),
            coreConfig: $this->objectManager->get(
                type: CoreConfig::class
            ),
            encryptor: $this->objectManager->get(
                type: EncryptorInterface::class
            )
        );
    }
}
