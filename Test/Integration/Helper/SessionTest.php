<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Mapi\Test\Integration\Helper;

use Magento\Framework\ObjectManagerInterface;
use PHPUnit\Framework\TestCase;
use Magento\TestFramework\Helper\Bootstrap;
use Resursbank\Mapi\Helper\Session;

/**
 * Integration test for Helper\Session.
 */
class SessionTest extends TestCase
{
    /** @var ObjectManagerInterface  */
    private $objectManager;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        $this->objectManager = Bootstrap::getObjectManager();
    }

    /**
     * Test clearSession method.
     *
     * @return void
     */
    public function testClearSession(): void
    {
        $sessionHelper = $this->getSessionHelper();
        $url = 'foobar';

        $sessionHelper->setPaymentSigningUrl(url: $url);

        static::assertEquals(
            expected: $url,
            actual: $sessionHelper->getPaymentSigningUrl()
        );

        $sessionHelper->clearSession();

        static::assertEmpty(
            actual: $sessionHelper->getPaymentSigningUrl()
        );
    }

    /**
     * Test set-, get- and unsetPaymentSigningUrl methods.
     *
     * @return void
     */
    public function testSetGetAndUnsetPaymentSigningUrl(): void
    {
        $sessionHelper = $this->getSessionHelper();
        $url = 'foobar';

        $sessionHelper->setPaymentSigningUrl(url: $url);

        static::assertEquals(
            expected: $url,
            actual: $sessionHelper->getPaymentSigningUrl()
        );

        $sessionHelper->unsetPaymentSigningUrl();

        static::assertEmpty(
            actual: $sessionHelper->getPaymentSigningUrl()
        );
    }

    /**
     * Fetch Session helper object.
     *
     * @return Session
     */
    private function getSessionHelper(): Session
    {
        return $this->objectManager->get(type: Session::class);
    }
}
