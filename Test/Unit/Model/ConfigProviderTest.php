<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Mapi\Test\Unit\Model;

use PHPUnit\Framework\TestCase;
use Magento\Quote\Model\Quote;
use Magento\Store\Model\Store;
use Resursbank\Core\Model\PaymentMethod;
use Resursbank\Mapi\Model\ConfigProvider;
use Resursbank\Mapi\Helper\Log;
use Resursbank\Core\Helper\PaymentMethods\Ecom as PaymentMethods;
use Resursbank\Core\ViewModel\Session\Checkout as CheckoutSession;
use Resursbank\Core\Helper\PaymentMethods as CorePaymentMethods;

/**
 * Tests for ConfigProvider model class.
 */
class ConfigProviderTest extends TestCase
{
    /**
     * Verify that getConfig returns correct data.
     *
     * @return void
     */
    public function testGetConfig(): void
    {
        $configProvider = $this->getConfigProvider();
        $config = $configProvider->getConfig();

        static::assertArrayHasKey(
            key: 'payment',
            array: $config
        );
        static::assertIsArray(
            actual: $config['payment']
        );
        static::assertArrayHasKey(
            key: 'resursbank_mapi',
            array: $config['payment']
        );
        static::assertIsArray(
            actual: $config['payment']['resursbank_mapi']
        );
        static::assertArrayHasKey(
            key: 'methods',
            array: $config['payment']['resursbank_mapi']
        );
        static::assertIsArray(
            actual: $config['payment']['resursbank_mapi']['methods']
        );
        static::assertCount(
            expectedCount: 2,
            haystack: $config['payment']['resursbank_mapi']['methods']
        );
    }

    /**
     * Fetch a ConfigProvider object.
     *
     * @return ConfigProvider
     */
    private function getConfigProvider(): ConfigProvider
    {
        $helper = $this->createMock(originalClassName: PaymentMethods::class);
        $helper->method('getMethods')->willReturn(value: [
                'default-method1' => $this->createMock(
                    originalClassName: PaymentMethod::class
                ),
                'default-method2' => $this->createMock(
                    originalClassName: PaymentMethod::class
                )
        ]);

        $session = $this->createMock(originalClassName: CheckoutSession::class);
        $quote = $this->createMock(originalClassName: Quote::class);
        $store = $this->createMock(originalClassName: Store::class);
        $store->method('getCode')->willReturn(value: 'store1');
        $quote->method('getStore')->willReturn(value: $store);
        $session->method('getQuote')->willReturn(value: $quote);

        return new ConfigProvider(
            log: $this->createMock(originalClassName: Log::class),
            helper: $helper,
            session: $session,
            corePaymentMethods: $this->createMock(
                originalClassName: CorePaymentMethods::class
            ),
        );
    }
}
