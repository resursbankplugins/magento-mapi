<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Mapi\Test\Unit\ViewModel;

use PHPUnit\Framework\TestCase;
use Resursbank\Mapi\ViewModel\FetchAddress;
use Magento\Framework\App\ProductMetadataInterface;
use Magento\Framework\Data\Form\FormKey;
use Magento\Framework\UrlInterface;
use Resursbank\Mapi\Helper\Log;

/**
 * Unit tests for ViewModel\FetchAddress.
 */
class FetchAddressTest extends TestCase
{
    /**
     * Verify that isOldMagento returns true for versions lower than 2.4.7.
     *
     * @return void
     */
    public function testIsOldMagento(): void
    {
        for ($i = 0; $i < 3; $i ++) {
            for ($j = 0; $j < 12; $j ++) {
                static::assertTrue(
                    condition: $this->getFetchAddress(
                        version: '2.' . $i . '.' . $j
                    )->isOldMagento()
                );
            }
        }

        static::assertFalse(
            condition: $this->getFetchAddress(version: '2.4.7')->isOldMagento()
        );
    }

    /**
     * Get FetchAddress object.
     *
     * @param string $version Return value of productMetadata::getVersion().
     * @return FetchAddress
     */
    private function getFetchAddress(string $version): FetchAddress
    {
        $productMetadata = $this->createMock(
            originalClassName: ProductMetadataInterface::class
        );
        $productMetadata->method('getVersion')
            ->willReturn(value: $version);

        return new FetchAddress(
            formKey: $this->createMock(originalClassName: FormKey::class),
            log: $this->createMock(originalClassName: Log::class),
            url: $this->createMock(originalClassName: UrlInterface::class),
            productMetadata: $productMetadata
        );
    }
}
