<?php /** @noinspection PhpDeprecationInspection */
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Mapi\Test\Unit\Helper;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use Magento\Sales\Model\Order as OrderModel;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Sales\Api\OrderManagementInterface;
use Magento\Sales\Api\TransactionRepositoryInterface;
use Magento\Framework\Api\FilterBuilder;
use Magento\Sales\Model\OrderFactory;
use Resursbank\Ecom\Lib\Validation\StringValidation;
use Resursbank\Mapi\Helper\Order;
use Resursbank\Mapi\Helper\Config as ConfigHelper;
use Resursbank\Core\Helper\Log as LogHelper;

/**
 * Tests for the Helper\Order class.
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class OrderTest extends TestCase
{
    /**
     * Verify that the isMapi method works as intended.
     *
     * Should return true if the order passed to it has its 'resursbank_flow'
     * attribute set to the value of
     * Resursbank\Mapi\Helper\Config::API_FLOW_OPTION.
     *
     * @return void
     * @todo Rewrite this without ObjectManager
     * @noinspection PhpParamsInspection
     */
    public function testIsMapi(): void
    {
        $orderHelper = $this->getOrderHelper();

        $objectManager = new ObjectManager($this);
        $order = $objectManager->getObject(className: OrderModel::class);

        $order->setData(
            key: 'resursbank_flow',
            value: ConfigHelper::API_FLOW_OPTION
        );
        static::assertTrue(
            condition: $orderHelper->isMapi(order: $order)
        );

        $order->setData(key: 'resursbank_flow', value: '');
        static::assertFalse(
            condition: $orderHelper->isMapi(order: $order)
        );
    }

    /**
     * Fetch an Order helper object instance.
     *
     * @return Order
     */
    private function getOrderHelper(): Order
    {
        return new Order(
            context: $this->createMock(originalClassName: Context::class),
            orderRepo: $this->createMock(
                originalClassName: OrderRepositoryInterface::class
            ),
            request: $this->createMock(
                originalClassName: RequestInterface::class
            ),
            orderManagement: $this->createMock(
                originalClassName: OrderManagementInterface::class
            ),
            log: $this->createMock(originalClassName: LogHelper::class),
            transactionRepository: $this->createMock(
                originalClassName: TransactionRepositoryInterface::class
            ),
            filterBuilder: $this->createMock(
                originalClassName: FilterBuilder::class
            ),
            searchCriteriaBuilder: $this->createMock(
                originalClassName: SearchCriteriaBuilder::class
            ),
            stringValidation: $this->createMock(
                originalClassName: StringValidation::class
            ),
            orderFactory: $this->createMock(
                originalClassName: OrderFactory::class
            )
        );
    }
}
