<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Mapi\Test\Unit\Helper;

use PHPUnit\Framework\TestCase;
use Resursbank\Mapi\Helper\Gateway as GatewayHelper;
use Resursbank\Ordermanagement\Helper\Config as OrdermanagementConfig;
use Resursbank\Core\Gateway\Data\Order\OrderAdapter;
use Resursbank\Mapi\Helper\Order as OrderHelper;
use Resursbank\Core\Helper\Order as CoreOrderHelper;
use Resursbank\Mapi\Helper\Log as LogHelper;
use Resursbank\Mapi\Helper\Config as ConfigHelper;
use Magento\Store\Model\Store;

/**
 * Tests for the Helper\Gateway class.
 */
class GatewayTest extends TestCase
{
    /** @var GatewayHelper */
    private GatewayHelper $gateway;

    /** @var LogHelper */
    private LogHelper $logHelper;

    /** @var ConfigHelper */
    private ConfigHelper $configHelper;

    /** @var OrderHelper */
    private OrderHelper $orderHelper;

    /** @var OrdermanagementConfig */
    private OrderManagementConfig $orderManagementConfig;

    /** @var CoreOrderHelper */
    private CoreOrderHelper $coreOrderHelper;

    /** @var OrderAdapter */
    private OrderAdapter $orderAdapter;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        $this->logHelper = $this->createMock(
            originalClassName: LogHelper::class
        );

        $this->configHelper = $this->createMock(
            originalClassName: ConfigHelper::class
        );
        $this->configHelper->method('isActive')->willReturn(
            value: true
        );

        $this->orderHelper = $this->createMock(
            originalClassName: OrderHelper::class
        );
        $this->orderHelper->method('isMapi')->willReturn(value: true);

        $this->orderManagementConfig = $this->createMock(
            originalClassName: OrdermanagementConfig::class
        );
        $this->orderManagementConfig->method('isAfterShopEnabled')
            ->willReturn(value: true);

        $this->coreOrderHelper = $this->createMock(
            originalClassName: CoreOrderHelper::class
        );

        $store = $this->createMock(Store::class);
        $store->method('getCode')->willReturn(
            value: 'default'
        );

        $this->orderAdapter = $this->createMock(OrderAdapter::class);
        $this->orderAdapter->method('getStore')->willReturn(
            value: $store
        );
    }

    /**
     * Verify that isEnabled returns true when the order is for the  MAPI flow.
     *
     * @return void
     */
    public function testIsEnabledWithMapiFlow(): void
    {
        $this->gateway = $this->getGateway();

        static::assertTrue(
            condition: $this->gateway->isEnabled(entity: $this->orderAdapter)
        );
    }

    /**
     * Verify that isEnabled returns true for processable legacy orders.
     *
     * @return void
     */
    public function testIsEnabledWithProcessableLegacy(): void
    {
        $this->orderHelper = $this->createMock(
            originalClassName: OrderHelper::class
        );
        $this->orderHelper->method('isMapi')->willReturn(
            value: false
        );

        $this->coreOrderHelper = $this->createMock(
            originalClassName: CoreOrderHelper::class
        );
        $this->coreOrderHelper->method('isProcessableLegacy')
            ->willReturn(value: true);

        $this->gateway = $this->getGateway();

        static::assertTrue(
            condition: $this->gateway->isEnabled(entity: $this->orderAdapter)
        );
    }

    /**
     * Verify that isEnabled returns false if MAPI flow is inactive.
     *
     * @return void
     */
    public function testIsEnabledFailsIfMapiFlowInactive(): void
    {
        $this->configHelper = $this->createMock(
            originalClassName: ConfigHelper::class
        );
        $this->configHelper->method('isActive')->willReturn(
            value: false
        );

        $this->gateway = $this->getGateway();

        static::assertFalse(
            condition: $this->gateway->isEnabled(entity: $this->orderAdapter)
        );
    }

    /**
     * Verify that isEnabled returns false if after-shop features are disabled.
     *
     * @return void
     */
    public function testIsEnabledFailsIfAfterShopDisabled(): void
    {
        $this->orderManagementConfig = $this->createMock(
            originalClassName: OrdermanagementConfig::class
        );
        $this->orderManagementConfig->method('isAfterShopEnabled')
            ->willReturn(value: false);

        $this->gateway = $this->getGateway();

        static::assertFalse(
            condition: $this->gateway->isEnabled(entity: $this->orderAdapter)
        );
    }

    /**
     * Fetch Gateway helper object.
     *
     * @return GatewayHelper
     */
    private function getGateway(): GatewayHelper
    {
        return new GatewayHelper(
            ordermanagementConfig: $this->orderManagementConfig,
            orderHelper: $this->orderHelper,
            coreOrderHelper: $this->coreOrderHelper,
            log: $this->logHelper,
            config: $this->configHelper
        );
    }
}
