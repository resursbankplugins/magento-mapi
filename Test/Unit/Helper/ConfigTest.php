<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Mapi\Test\Unit\Helper;

use PHPUnit\Framework\TestCase;
use Resursbank\Mapi\Helper\Config as ConfigHelper;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\App\Helper\Context;
use Resursbank\Core\Helper\Config as CoreConfig;
use Magento\Framework\Encryption\EncryptorInterface;

/**
 * Tests for the Helper\Config class.
 */
class ConfigTest extends TestCase
{

    /** @var Context */
    private Context $contextMock;

    /** @var WriterInterface */
    private WriterInterface $writerMock;

    /** @var EncryptorInterface */
    private EncryptorInterface $encryptorMock;

    /** @var ScopeConfigInterface */
    private scopeConfigInterface $scopeConfigMock;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        $this->contextMock = $this->createMock(
            originalClassName: Context::class
        );
        $this->writerMock = $this->createMock(
            originalClassName: WriterInterface::class
        );
        $this->encryptorMock = $this->createMock(
            originalClassName: EncryptorInterface::class
        );
        $this->scopeConfigMock = $this->createMock(
            originalClassName: ScopeConfigInterface::class
        );
    }

    /**
     * Verify that the isActive method behaves as intended.
     *
     * @return void
     */
    public function testIsActive(): void
    {
        $coreConfig = $this->createMock(
            originalClassName: CoreConfig::class
        );
        $coreConfig->method('getFlow')->willReturn(value: 'mapi');

        $config = new ConfigHelper(
            reader: $this->scopeConfigMock,
            writer: $this->writerMock,
            context: $this->contextMock,
            coreConfig: $coreConfig,
            encryptor: $this->encryptorMock
        );

        static::assertTrue(condition: $config->isActive());

        $coreConfig = $this->createMock(CoreConfig::class);
        $coreConfig->method('getFlow')->willReturn(
            value: 'simplified'
        );
        $config = new ConfigHelper(
            reader: $this->scopeConfigMock,
            writer: $this->writerMock,
            context: $this->contextMock,
            coreConfig: $coreConfig,
            encryptor: $this->encryptorMock
        );

        static::assertFalse(condition: $config->isActive());
    }
}
