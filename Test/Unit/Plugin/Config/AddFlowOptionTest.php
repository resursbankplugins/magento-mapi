<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Mapi\Test\Unit\Plugin\Config;

use PHPUnit\Framework\TestCase;
use Resursbank\Core\Model\Config\Source\Flow;
use Resursbank\Mapi\Plugin\Config\AddFlowOption;

/**
 * Unit test for AddFlowOption plugin.
 */
class AddFlowOptionTest extends TestCase
{
    /**
     * Verify that data gets added correctly.
     *
     * @return void
     */
    public function testAfterToArray(): void
    {
        $addFlowOption = new AddFlowOption();

        $inData = ['key' => 'value'];

        $flow = $this->createMock(originalClassName: Flow::class);

        $result = $addFlowOption->afterToArray(
            subject: $flow,
            result: $inData
        );

        static::assertArrayHasKey(
            key: 'key',
            array: $result
        );
        static::assertEquals(
            expected: $inData['key'],
            actual: $result['key']
        );
    }
}
