<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Mapi\Test\Unit\Plugin\Ordermanagement\Helper;

use PHPUnit\Framework\TestCase;
use Resursbank\Mapi\Plugin\Ordermanagement\Helper\Config;
use Resursbank\Mapi\Helper\Config as ConfigHelper;
use Resursbank\Ordermanagement\Helper\Config as Subject;

/**
 * Unit tests for Plugin\Ordermanagement\Helper\Config.
 */
class ConfigTest extends TestCase
{
    /**
     * Verify that afterIsAutoInvoiceEnabled always returns true when active.
     *
     * @return void
     */
    public function testAfterIsAutoInvoiceEnabled(): void
    {
        $config = $this->getConfig(isActive: true);
        static::assertTrue(condition: $config->afterIsAutoInvoiceEnabled(
            subject: $this->createMock(originalClassName: Subject::class),
            result: false,
            scopeCode: 'foo'
        ));

        $config = $this->getConfig(isActive: false);
        static::assertFalse(condition: $config->afterIsAutoInvoiceEnabled(
            subject: $this->createMock(originalClassName: Subject::class),
            result: false,
            scopeCode: 'foo'
        ));
    }

    /**
     * Fetch a Config object.
     *
     * @param bool $isActive Desired output of ConfigHelper::isActive
     * @return Config
     */
    private function getConfig(bool $isActive): Config
    {
        $configHelper = $this->createMock(
            originalClassName: ConfigHelper::class
        );
        $configHelper->method('isActive')
            ->willReturn(value: $isActive);
        return new Config(
            config: $configHelper,
        );
    }
}
