<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Mapi\Test\Unit\Plugin\Core\Plugin\Payment\Helper;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use PHPUnit\Framework\TestCase;
use Resursbank\Mapi\Plugin\Core\Plugin\Payment\Helper\Data;
use Resursbank\Core\Plugin\Payment\Helper\Data as Subject;
use Resursbank\Core\Helper\Scope;
use Resursbank\Mapi\Helper\Config;

/**
 * Tests for the Plugin\Core\Plugin\Payment\Helper\Data plugin.
 */
class DataTest extends TestCase
{
    /** @var Config */
    private Config $config;

    /** @var Scope */
    private Scope $scope;

    /**
     * @inheritDoc
     *
     * @return void
     */
    protected function setUp(): void
    {
        $this->config = $this->createMock(originalClassName: Config::class);
        $this->scope = $this->createMock(originalClassName: Scope::class);
    }

    /**
     * Verify that plugin returns true if flow is active.
     *
     * @return void
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function testAroundSwishMaxOrderLimitApplicableWhenActive(): void
    {
        $this->config->method('isActive')->willReturn(value: true);
        $data = new Data(config: $this->config, scope: $this->scope);

        static::assertTrue(
            condition: $data->aroundSwishMaxOrderLimitApplicable(
                subject: $this->createMock(originalClassName: Subject::class),
                proceed: $this->proceed(...)
            )
        );
    }

    /**
     * Verify that value of $proceed is used if plugin is inactive.
     *
     * @return void
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function testAroundSwishMaxOrderLimitApplicableWhenNotActive(): void
    {
        $this->config->method('isActive')->willReturn(value: false);
        $data = new Data(config: $this->config, scope: $this->scope);

        static::assertFalse(
            condition: $data->aroundSwishMaxOrderLimitApplicable(
                subject: $this->createMock(originalClassName: Subject::class),
                proceed: $this->proceed(...)
            )
        );
    }

    /**
     * Represents the $proceed argument for aroundSwishMaxOrderLimitApplicable.
     *
     * Always returns false to contrast with the always-true response when the
     * MAPI flow is active.
     *
     * @return bool
     */
    private function proceed(): bool
    {
        return false;
    }
}
