<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Mapi\Test\Unit\Plugin\Core\Block\Adminhtml\System\Config;

use PHPUnit\Framework\TestCase;
use Resursbank\Mapi\Plugin\Core\Block\Adminhtml\System\Config\SupportInfo;
use Resursbank\Core\Block\Adminhtml\System\Config\SupportInfo as Subject;
use Resursbank\Mapi\Helper\Log;
use Magento\Framework\Module\PackageInfo;

/**
 * Unit test for SupportInfo plugin.
 */
class SupportInfoTest extends TestCase
{
    /**
     * Verify that module name and version are correctly appended.
     *
     * @return void
     */
    public function testAfterGetVersion(): void
    {
        $packageInfo = $this->createMock(originalClassName: PackageInfo::class);
        $version = '1.2.3';
        $packageInfo->method('getVersion')
            ->willReturn(value: $version);
        $supportInfo = new SupportInfo(
            packageInfo: $packageInfo,
            log: $this->createMock(originalClassName: Log::class)
        );

        $oldResult = 'foo';
        $result = $supportInfo->afterGetVersion(
            subject: $this->createMock(originalClassName: Subject::class),
            result: $oldResult
        );

        static::assertEquals(
            expected: $oldResult . '<br />Resursbank_Mapi: ' . $version,
            actual: $result
        );
    }
}
