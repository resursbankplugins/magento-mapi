<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Mapi\Test\Unit\Plugin\Core\Block\Adminhtml\System\Config;

use PHPUnit\Framework\TestCase;
use Resursbank\Mapi\Plugin\Core\Block\Adminhtml\System\Config\GetStores;
use Resursbank\Core\Block\Adminhtml\System\Config\GetStores as Subject;
use Resursbank\Core\Helper\Url;
use Resursbank\Core\Helper\Log;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Data\Form\FormKey;

/**
 * Unit test for GetStores plugin.
 */
class GetStoresTest extends TestCase
{
    /**
     * Verify that afterGetUrls properly modifies the input data.
     *
     * @return void
     */
    public function testAfterGetUrls(): void
    {
        $getStores = new GetStores();

        $formKey = $this->createMock(FormKey::class);
        $formKey->method('getFormKey')
            ->willReturn(value: '123456789');
        $context = $this->createMock(originalClassName: Context::class);
        $context->method('getFormKey')
            ->willReturn(value: $formKey);
        $url = $this->createMock(Url::class);
        $adminUrlValue = 'bar';
        $url->method('getAdminUrl')
            ->willReturn(value: $adminUrlValue);
        $subject = new Subject(
            context: $context,
            log: $this->createMock(Log::class),
            url: $url
        );
        $result = $getStores->afterGetUrls(
            subject: $subject,
            result: [
                'simplified' => 'foo'
            ]
        );

        static::assertEquals(
            expected: $adminUrlValue,
            actual: $result['mapi']
        );
    }
}
