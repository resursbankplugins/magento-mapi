<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Mapi\Test\Unit\Plugin\Core\Helper;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use PHPUnit\Framework\TestCase;
use Resursbank\Core\Helper\Ecom as Subject;
use Resursbank\Core\Helper\Scope;
use Resursbank\Core\Helper\Version;
use Resursbank\Ecom\Lib\Api\Scope as EcomScope;
use Resursbank\Ecom\Lib\Api\Environment;
use Resursbank\Mapi\Plugin\Core\Helper\Ecom;
use Resursbank\Mapi\Helper\Config;
use Resursbank\Mapi\Helper\Log;

/**
 * Unit tests for the Core\Helper\Ecom plugin.
 */
class EcomTest extends TestCase
{
    /** @var Config */
    private Config $config;

    /** @var Log */
    private Log $log;

    /** @var Scope */
    private Scope $scope;

    /** @var Version */
    private Version $version;

    /**
     * @inheritDoc
     *
     * @return void
     */
    protected function setUp(): void
    {
        $this->config = $this->createMock(originalClassName: Config::class);
        $this->log = $this->createMock(originalClassName: Log::class);
        $this->scope = $this->createMock(originalClassName: Scope::class);
        $this->version = $this->createMock(originalClassName: Version::class);
    }

    /**
     * Verify that afterCanConnect works as intended when MAPI is active.
     *
     * @return void
     */
    public function testAfterCanConnectWhenActive(): void
    {
        $this->config->method('isActive')
            ->willReturn(true);

        $ecom = new Ecom(
            config: $this->config,
            log: $this->log,
            scope: $this->scope,
            version: $this->version
        );

        static::assertTrue(
            condition: $ecom->afterCanConnect(
                subject: $this->createMock(originalClassName: Subject::class),
                result: false
            )
        );
        static::assertTrue(
            condition: $ecom->afterCanConnect(
                subject: $this->createMock(originalClassName: Subject::class),
                result: true
            )
        );
    }

    /**
     * Verify that afterCanConnect works as intended when MAPI isn't active.
     *
     * @return void
     */
    public function testAfterCanConnectWhenNotActive(): void
    {
        $this->config->method('isActive')
            ->willReturn(false);

        $ecom = new Ecom(
            config: $this->config,
            log: $this->log,
            scope: $this->scope,
            version: $this->version
        );

        static::assertFalse(
            condition: $ecom->afterCanConnect(
                subject: $this->createMock(originalClassName: Subject::class),
                result: false
            )
        );
        static::assertTrue(
            condition: $ecom->afterCanConnect(
                subject: $this->createMock(originalClassName: Subject::class),
                result: true
            )
        );
    }

    /**
     * Verify that getScope always returns true when MAPI is active.
     *
     * @return void
     */
    public function testAfterGetScopeWhenActive(): void
    {
        $this->config->method('isActive')
            ->willReturn(value: true);

        $ecom = new Ecom(
            config: $this->config,
            log: $this->log,
            scope: $this->scope,
            version: $this->version
        );

        static::assertEquals(
            expected: EcomSCope::MERCHANT_API,
            actual: $ecom->afterGetScope(
                subject: $this->createMock(originalClassName: Subject::class),
                result: EcomScope::MERCHANT_API,
                environment: Environment::TEST
            )
        );

        foreach (EcomSCope::cases() as $scope) {
            foreach (Environment::cases() as $environment) {
                static::assertEquals(
                    expected: EcomSCope::MERCHANT_API,
                    actual: $ecom->afterGetScope(
                        subject: $this->createMock(originalClassName: Subject::class),
                        result: $scope,
                        environment: $environment
                    )
                );
            }
        }
    }

    /**
     * Verify that getScope never modifies $result when MAPI isn't active.
     *
     * @return void
     */
    public function testAfterGetScopeWhenNotActive(): void
    {
        $this->config->method('isActive')
            ->willReturn(value: false);

        $ecom = new Ecom(
            config: $this->config,
            log: $this->log,
            scope: $this->scope,
            version: $this->version
        );

        static::assertEquals(
            expected: EcomSCope::MERCHANT_API,
            actual: $ecom->afterGetScope(
                subject: $this->createMock(originalClassName: Subject::class),
                result: EcomScope::MERCHANT_API,
                environment: Environment::TEST
            )
        );

        foreach (EcomSCope::cases() as $scope) {
            foreach (Environment::cases() as $environment) {
                static::assertEquals(
                    expected: $scope,
                    actual: $ecom->afterGetScope(
                        subject: $this->createMock(originalClassName: Subject::class),
                        result: $scope,
                        environment: $environment
                    )
                );
            }
        }
    }

    /**
     * Verify that afterGetUserAgent properly appends module info to $result.
     *
     * @return void
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function testAfterGetUserAgentWhenActive(): void
    {
        $this->config->method('isActive')
            ->willReturn(value: true);
        $version = '9.8.7';
        $this->version->method('getComposerVersion')
            ->willReturn(value: $version);

        $ecom = new Ecom(
            config: $this->config,
            log: $this->log,
            scope: $this->scope,
            version: $this->version
        );

        $result = 'Resursbank_Core 1.2.3 |';

        static::assertEquals(
            expected: $result . ' Resursbank_Mapi ' . $version . ' |',
            actual: $ecom->afterGetUserAgent(
                subject: $this->createMock(originalClassName: Subject::class),
                result: $result
            )
        );
    }

    /**
     * Verify that afterGetUserAgent doesn't modify $result if module inactive.
     *
     * @return void
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function testAfterGetUserAgentWhenNotActive(): void
    {
        $this->config->method('isActive')
            ->willReturn(value: false);

        $ecom = new Ecom(
            config: $this->config,
            log: $this->log,
            scope: $this->scope,
            version: $this->version
        );

        $result = 'Resursbank_Core 1.2.3 |';
        static::assertEquals(
            expected: $result,
            actual: $ecom->afterGetUserAgent(
                subject: $this->createMock(originalClassName: Subject::class),
                result: $result
            )
        );
    }
}
