<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Mapi\Test\Unit\Plugin\Core\Helper;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Resursbank\Mapi\Plugin\Core\Helper\Url;
use Resursbank\Core\Helper\Url as Subject;
use Resursbank\Core\Helper\Scope;
use Resursbank\Mapi\Helper\Config;

use PHPUnit\Framework\TestCase;

/**
 * Unit test for the Core\Helper\Url plugin.
 */
class UrlTest extends TestCase
{
    /**
     * Verify that afterGetCheckoutRebuildRedirectUrl behaves properly.
     *
     * Tested method should always return input parameter $result unmodified
     * if the MAPI flow is disabled and the same string with '/#payment'
     * appended if the flow is active.
     *
     * @return void
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function testAfterGetCheckoutRebuildRedirectUrl(): void
    {
        $inputResult = 'foo';

        static::assertEquals(
            expected: $inputResult . '/#payment',
            actual: $this->getUrl(
                isActive: true
            )->afterGetCheckoutRebuildRedirectUrl(
                subject: $this->createMock(originalClassName: Subject::class),
                result: $inputResult
            )
        );

        static::assertEquals(
            expected: $inputResult,
            actual: $this->getUrl(
                isActive: false
            )->afterGetCheckoutRebuildRedirectUrl(
                subject: $this->createMock(originalClassName: Subject::class),
                result: $inputResult
            )
        );
    }

    /**
     * Generate an Url object.
     *
     * @param bool $isActive Return value of config property's isActive method.
     * @return Url
     */
    private function getUrl(bool $isActive): Url
    {
        $config = $this->createMock(originalClassName: Config::class);
        $config->method('isActive')->willReturn(value: $isActive);

        return new Url(
            scope: $this->createMock(originalClassName: Scope::class),
            config: $config
        );
    }
}
