<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Mapi\Test\Unit\Plugin\Core\Helper;

use PHPUnit\Framework\TestCase;
use Resursbank\Mapi\Helper\Config as ConfigHelper;
use Resursbank\Core\Helper\Config as Subject;
use Resursbank\Mapi\Plugin\Core\Helper\Config;

/**
 * Unit tests for the Core\Helper\Config plugin.
 */
class ConfigTest extends TestCase
{
    /** @var Subject */
    private Subject $subject;

    /** @var ConfigHelper */
    private ConfigHelper $configHelper;

    /**
     * @inheritDoc
     *
     * @return void
     */
    protected function setUp(): void
    {
        $this->subject = $this->createMock(originalClassName: Subject::class);
        $this->configHelper = $this->createMock(
            originalClassName: ConfigHelper::class
        );
        $this->configHelper->method('getClientId')
            ->willReturn(value: 'mapi');
        $this->configHelper->method('getClientSecret')
            ->willReturn(value: 'secret');
    }

    /**
     * Verify output of aroundGetClientId when isActive returns true.
     *
     * @return void
     */
    public function testAroundGetClientIdWhenActive(): void
    {
        $this->configHelper->method('isActive')
            ->willReturn(value: true);
        $config = new Config(config: $this->configHelper);

        $result = $config->aroundGetClientId(
            subject: $this->subject,
            proceed: $this->proceedClientId(...),
            scopeCode: 'default'
        );

        static::assertEquals(
            expected: 'mapi',
            actual: $result
        );
    }

    /**
     * Verify output of aroundGetClientId when isActive returns false.
     *
     * @return void
     */
    public function testAroundGetClientIdWhenNotActive(): void
    {
        $this->configHelper->method('isActive')
            ->willReturn(value: false);
        $config = new Config(config: $this->configHelper);

        $result = $config->aroundGetClientId(
            subject: $this->subject,
            proceed: $this->proceedClientId(...),
            scopeCode: 'default'
        );

        static::assertEquals(
            expected: 'simplifiedId',
            actual: $result
        );
    }

    /**
     * Verify output of aroundGetClientSecret when isActive returns true.
     *
     * @return void
     */
    public function testAroundGetClientSecretWhenActive(): void
    {
        $this->configHelper->method('isActive')
            ->willReturn(value: true);
        $config = new Config(config: $this->configHelper);

        $result = $config->aroundGetClientSecret(
            subject: $this->subject,
            proceed: $this->proceedClientSecret(...),
            scopeCode: 'default'
        );

        static::assertEquals(
            expected: 'secret',
            actual: $result
        );
    }

    /**
     * Verify output of aroundGetClientSecret when isActive returns false.
     *
     * @return void
     */
    public function testAroundGetClientSecretWhenNotActive(): void
    {
        $this->configHelper->method('isActive')
            ->willReturn(value: false);
        $config = new Config(config: $this->configHelper);

        $result = $config->aroundGetClientSecret(
            subject: $this->subject,
            proceed: $this->proceedClientSecret(...),
            scopeCode: 'default'
        );

        static::assertEquals(
            expected: 'simplifiedSecret',
            actual: $result
        );
    }

    /**
     * Represents the $proceed argument for aroundGetClientId.
     *
     * @return string
     */
    private function proceedClientId(): string
    {
        return 'simplifiedId';
    }

    /**
     * Represents the $proceed argument for aroundGetClientSecret.
     *
     * @return string
     */
    private function proceedClientSecret(): string
    {
        return 'simplifiedSecret';
    }
}
