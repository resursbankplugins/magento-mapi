<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Mapi\Test\Unit\Plugin\Layout\File;

use PHPUnit\Framework\TestCase;
use Resursbank\Mapi\Plugin\Layout\File\Collector;
use Magento\Store\Model\Store;
use Magento\Framework\View\File;
use Magento\Framework\View\Layout\File\Collector\Aggregated;
use Magento\Store\Model\StoreManagerInterface;
use Resursbank\Mapi\Helper\Config;
use Resursbank\Mapi\Helper\Log;

/**
 * Unit test for Plugin\Layout\File\Collector class.
 */
class CollectorTest extends TestCase
{
    /**
     * Verify that afterGetFiles only removes files from our module.
     *
     * Additionally, files should only be removed if our module is active.
     *
     * @return void
     */
    public function testAfterGetFiles(): void
    {
        foreach ([true, false] as $isActive) {
            foreach ([true, false] as $isOurModule) {
                $collector = $this->getCollector(isActive: $isActive);
                $inputResult = $this->getResultData(isOurModule: $isOurModule);

                $result = $collector->afterGetFiles(
                    subject: $this->createMock(
                        originalClassName: Aggregated::class
                    ),
                    result: $inputResult
                );

                if (!$isActive && $isOurModule) {
                    static::assertArrayNotHasKey(
                        key: 'foo',
                        array: $result
                    );
                } else {
                    static::assertArrayHasKey(
                        key: 'foo',
                        array: $result
                    );
                }
            }
        }
    }

    /**
     * Fetches $result array.
     *
     * @param bool $isOurModule
     * @return array
     */
    private function getResultData(bool $isOurModule): array
    {
        $file = $this->createMock(originalClassName: File::class);

        if ($isOurModule) {
            $file->method('getModule')
                ->willReturn(value: 'Resursbank_Mapi');
        } else {
            $file->method('getModule')
                ->willReturn(value: 'Resursbank_Core');
        }

        return [
            'foo' => $file
        ];
    }

    /**
     * Fetches collector object
     *
     * @param bool $isActive Desired return value of config->isActive.
     * @return Collector
     */
    private function getCollector(
        bool $isActive
    ): Collector {
        $config = $this->createMock(originalClassName: Config::class);
        $config->method('isActive')
            ->willReturn(value: $isActive);
        $store = $this->createMock(originalClassName: Store::class);
        $store->method('getCode')
            ->willReturn(value: 'default');
        $storeManager = $this->createMock(
            originalClassName: StoreManagerInterface::class
        );
        $storeManager->method('getStore')
            ->willReturn(value: $store);
        $log = $this->createMock(Log::class);

        return new Collector(
            config: $config,
            storeManager: $storeManager,
            log: $log
        );
    }
}
