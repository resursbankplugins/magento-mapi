<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Mapi\Test\Unit\Plugin\Layout;

use PHPUnit\Framework\TestCase;
use Resursbank\Mapi\Plugin\Layout\Layout;
use Exception;
use Magento\Checkout\Block\Checkout\LayoutProcessor;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Store\Model\Store;
use Resursbank\Core\Helper\PaymentMethods\Ecom as PaymentMethods;
use Resursbank\Mapi\Helper\Log;
use Resursbank\Core\Model\PaymentMethod;

/**
 * Unit tests for Plugin\Layout\Layout.
 */
class LayoutTest extends TestCase
{
    /**
     * Verify that the beforeProcess method works as intended.
     *
     * @return void
     * @throws Exception
     */
    public function testBeforeProcess(): void
    {
        $validResult = $this->getValidResult();
        $invalidResult = $this->getInvalidResult();

        $layout = $this->getLayout();

        static::assertEquals(
            expected: [$invalidResult],
            actual: $layout->beforeProcess(
                subject: $this->createMock(
                    originalClassName: LayoutProcessor::class
                ),
                result: $invalidResult
            )
        );

        static::assertNotEquals(
            expected: [$validResult],
            actual: $layout->beforeProcess(
                subject: $this->createMock(
                    originalClassName: LayoutProcessor::class
                ),
                result: $validResult
            )
        );

        static::assertEquals(
            expected: $this->getValidOutputReference(),
            actual: $layout->beforeProcess(
                subject: $this->createMock(
                    originalClassName: LayoutProcessor::class
                ),
                result: $validResult
            )
        );
    }

    /**
     * Gets a valid $result array which will cause processing to proceed.
     *
     * @return array[]
     */
    private function getValidResult(): array
    {
        return [
            'components' => [
                'checkout' => [
                    'children' => [
                        'steps' => [
                            'children' => [
                                'billing-step' => [
                                    'children' => [
                                        'payment' => [
                                            'children' => [
                                                'foo' => 'bar'
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];
    }

    /**
     * Gets an invalid $result array which will cause processing to be aborted.
     *
     * @return array[]
     */
    private function getInvalidResult(): array
    {
        return [
            'components' => [
                'checkout' => [
                    'children' => [
                        'steps' => [
                            'children' => [
                                'billing-step' => [
                                    'children' => [
                                        'payment' => [
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];
    }

    /**
     * Gets the expected beforeProcess output for use with getValidResult.
     *
     * @return array[]
     */
    private function getValidOutputReference(): array
    {
        return [[
            'components' => [
                'checkout' => [
                    'children' => [
                        'steps' => [
                            'children' => [
                                'billing-step' => [
                                    'children' => [
                                        'payment' => [
                                            'children' => [
                                                'foo' => 'bar',
                                                'renders' => [
                                                    'children' => [
                                                        'resursbank-mapi' => [
                                                            'methods' => [
                                                                'foo' => [
                                                                    'isBillingAddressRequired' => true
                                                                ]
                                                            ]
                                                        ]
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ]];
    }

    /**
     * Fetches a Layout object.
     *
     * @return Layout
     */
    private function getLayout(): Layout
    {
        $helper = $this->createMock(originalClassName: PaymentMethods::class);
        $method = $this->createMock(originalClassName: PaymentMethod::class);
        $method->method('getCode')
            ->willReturn('foo');
        $helper->method('getMethods')->willReturn(value: [$method]);

        $storeManager = $this->createMock(
            originalClassName: StoreManagerInterface::class
        );
        $store = $this->createMock(originalClassName: Store::class);
        $store->method('getCode')
            ->willReturn(value: 'foo');
        $storeManager->method('getStore')
            ->willReturn(value: $store);

        return new Layout(
            log: $this->createMock(originalClassName: Log::class),
            helper: $helper,
            storeManager: $storeManager
        );
    }
}
