<?php

/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Mapi\Plugin\Layout\File;

use Exception;
use Magento\Framework\View\File;
use Magento\Framework\View\Layout\File\Collector\Aggregated;
use Magento\Store\Model\StoreManagerInterface;
use Resursbank\Mapi\Helper\Config;
use Resursbank\Mapi\Helper\Log;

use function strpos;

/**
 * This plugin will exclude our overriding layout file for checkout_index_index
 * if the module has been disabled.
 *
 * This plugin is necessary to ensure different API flows may be applied
 * individually in each store. Otherwise, the extending checkout_index_index.xml
 * files might conflict with each other.
 */
class Collector
{
    /**
     * @param Config $config
     * @param StoreManagerInterface $storeManager
     * @param Log $log
     */
    public function __construct(
        private readonly Config $config,
        private readonly StoreManagerInterface $storeManager,
        private readonly Log $log
    ) {
    }

    /**
     * Disable our checkout_index_index.xml file if this plugin is not enabled.
     *
     * @param Aggregated $subject
     * @param array $result
     * @return array<string, File>
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @noinspection PhpUnusedParameterInspection
     */
    public function afterGetFiles(
        Aggregated $subject,
        array $result
    ): array {
        try {
            $storeCode = $this->storeManager->getStore()->getCode();

            if (!$this->config->isActive(scopeCode: $storeCode)) {
                foreach ($result as $key => $file) {
                    if ($file->getModule() === 'Resursbank_Mapi') {
                        unset($result[$key]);
                    }
                }
            }
        } catch (Exception $e) {
            $this->log->exception($e);
        }

        return $result;
    }
}
