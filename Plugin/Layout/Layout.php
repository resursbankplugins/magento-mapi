<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Mapi\Plugin\Layout;

use Exception;
use Magento\Checkout\Block\Checkout\LayoutProcessor;
use Magento\Store\Model\StoreManagerInterface;
use Resursbank\Core\Exception\InvalidDataException;
use Resursbank\Core\Helper\PaymentMethods\Ecom as PaymentMethods;
use Resursbank\Mapi\Helper\Log;
use function is_string;

/**
 * Injects 'isBillingAddressRequired' property for all our payment methods in
 * the compiled layout XML. This is to ensure the billing address form section
 * is displayed for all our payment methods without us needing to specify the
 * requirement in the layout XML for each payment method (since the methods are
 * dynamically named for each account this is not a possibility for us).
 */
class Layout
{
    /**
     * @param Log $log
     * @param PaymentMethods $helper
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        private readonly Log $log,
        private readonly PaymentMethods $helper,
        private readonly StoreManagerInterface $storeManager
    ) {
    }

    /**
     * Modify layout before rendering.
     *
     * Here we make billing address required for our dynamic payment methods,
     * this cannot be deon statically through an XML file since we do not know
     * what payment methods are available at any given moment until we render
     * the actual checkout page.
     *
     * @param LayoutProcessor $subject
     * @param array $result
     * @return array<int, array>
     * @throws Exception
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @noinspection PhpUnusedParameterInspection
     */
    public function beforeProcess(
        LayoutProcessor $subject,
        array $result
    ): array {
        try {
            if (isset($result['components']['checkout']['children']['steps']
                ['children']['billing-step']['children']
                ['payment']['children'])
            ) {
                $methods = $this->helper->getMethods(
                    scopeCode: $this->storeManager->getStore()->getCode()
                );

                foreach ($methods as $method) {
                    $code = $method->getCode();

                    if (!is_string(value: $code)) {
                        new InvalidDataException(phrase: __(
                            'rb-payment-method-does-not-have-a-code'
                        ));
                    }

                    $result['components']['checkout']['children']['steps']
                    ['children']['billing-step']['children']['payment']
                    ['children']['renders']['children']['resursbank-mapi']
                    ['methods'][$code]
                    ['isBillingAddressRequired'] = true;
                }
            }
        } catch (Exception $e) {
            $this->log->exception(error: $e);
            throw $e;
        }

        return [$result];
    }
}
