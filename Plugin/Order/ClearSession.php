<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Mapi\Plugin\Order;

use Magento\Checkout\Controller\Onepage\Failure;
use Magento\Checkout\Controller\Onepage\Success;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\Page;
use Resursbank\Mapi\Helper\Session as SessionHelper;

/**
 * Clear this module's own session data after a successful order placement.
 */
class ClearSession
{
    /**
     * @param SessionHelper $sessionHelper
     */
    public function __construct(
        private readonly SessionHelper $sessionHelper
    ) {
    }

    /**
     * Clear session data.
     *
     * @param Success|Failure $subject
     * @param ResultInterface|Redirect|Page $result
     * @return ResultInterface|Redirect|Page
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @noinspection PhpUnusedParameterInspection
     */
    public function afterExecute(
        Success|Failure $subject,
        ResultInterface|Redirect|Page $result
    ): ResultInterface|Redirect|Page {
        $this->sessionHelper->clearSession();
        return $result;
    }
}
