<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Mapi\Plugin\Core\ViewModel;

use Resursbank\Core\ViewModel\Error as Subject;
use Resursbank\Mapi\Helper\Log;
use Resursbank\Ecom\Module\Payment\Repository as PaymentRepository;
use Resursbank\Mapi\Helper\Config;
use Resursbank\Core\Helper\Scope;
use Resursbank\Core\ViewModel\Session\Checkout as CheckoutSession;
use Throwable;

/**
 * MAPI-specific error handling.
 *
 * @SuppressWarnings(PHPMD.CookieAndSessionMisuse)
 */
class Error
{
    /** @var string */
    private string $error = '';

    /**
     * @param Config $config
     * @param Scope $scope
     * @param CheckoutSession $checkoutSession
     * @param Log $log
     */
    public function __construct(
        private readonly Config $config,
        private readonly Scope $scope,
        private readonly CheckoutSession $checkoutSession,
        private readonly Log $log
    ) {
    }

    /**
     * Handle rejected MAPI requests.
     *
     * Checks if we have a stored MAPI payment ID and if so loads a
     * relevant error message for later use.
     *
     * @param Subject $subject
     * @param callable $proceed
     * @return bool
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @noinspection PhpUnusedParameterInspection
     */
    public function aroundPaymentFailed(
        Subject $subject,
        callable $proceed
    ): bool {
        try {
            if ($this->config->isActive(scopeCode: $this->scope->getId())) {
                $order = $this->checkoutSession->getLastRealOrder();
                $paymentId = $order->getData(key: 'resursbank_mapi_payment_id');

                if (isset($paymentId) && $paymentId !== '') {
                    $payment = PaymentRepository::get(paymentId: $paymentId);
                    if ($payment->isRejected()) {
                        $this->error = $payment->rejectedReason
                            ->getFriendlyDescription() . ' ' .
                            __('rb-please-contact-resurs-bank-for-' .
                                'more-information');
                        return $proceed();
                    }

                    return false;
                }
            }
        } catch (Throwable $error) {
            $this->log->exception(error: $error);
        }

        return $proceed();
    }

    /**
     * Fetch MAPI-specific error.
     *
     * @param Subject $subject
     * @param string $result
     * @return string
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @noinspection PhpUnusedParameterInspection
     */
    public function afterGetError(
        Subject $subject,
        string $result
    ): string {
        try {
            if ($this->config->isActive(scopeCode: $this->scope->getId()) &&
                $this->error !== '') {
                return $this->error;
            }
        } catch (Throwable $error) {
            $this->log->exception(error: $error);
        }

        return $result;
    }
}
