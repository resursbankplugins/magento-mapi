<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Mapi\Plugin\Core\Helper;

use Magento\Sales\Api\Data\OrderInterface;
use Resursbank\Core\Helper\Order as Subject;
use Resursbank\Ecom\Exception\Validation\IllegalValueException;
use Resursbank\Ecom\Lib\Validation\StringValidation;
use Resursbank\Ecom\Module\Payment\Repository;
use Magento\Sales\Api\OrderRepositoryInterface;
use Resursbank\Mapi\Helper\Config;
use Resursbank\Core\Helper\Config as CoreConfig;
use Resursbank\Mapi\Helper\Log;
use Throwable;

/**
 * Interceptor for Resursbank\Core\Helper\Order.
 *
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 */
class Order
{
    /**
     * @param Config $configHelper
     * @param CoreConfig $coreConfigHelper
     * @param Log $log
     * @param StringValidation $stringValidation
     * @param OrderInterface $orderInterface
     * @param OrderRepositoryInterface $orderRepository
     */
    public function __construct(
        private readonly Config $configHelper,
        private readonly CoreConfig $coreConfigHelper,
        private readonly Log $log,
        private readonly StringValidation $stringValidation,
        private readonly OrderInterface $orderInterface,
        private readonly OrderRepositoryInterface $orderRepository
    ) {
    }

    /**
     * Use the MAPI API to fetch a legacy order's payment ID.
     *
     * @param Subject $subject
     * @param callable $proceed
     * @param OrderInterface $order
     * @return string
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @noinspection PhpUnusedParameterInspection
     */
    public function aroundGetPaymentId(
        Subject $subject,
        callable $proceed,
        OrderInterface $order
    ): string {
        if ($this->configHelper->isActive(scopeCode: $order->getStoreId())) {
            if ($order->getData(key: 'resursbank_mapi_payment_id')) {
                return $order->getData(key: 'resursbank_mapi_payment_id');
            }

            // No MAPI payment ID found on the order, we will attempt to locate
            // the order using the MAPI API.
            try {
                $result = Repository::search(
                    orderReference: $order->getIncrementId(),
                    storeId: $this->coreConfigHelper->getStore(
                        scopeCode: $order->getStoreId()
                    )
                );

                if (count(value: $result) > 0) {
                    $this->orderRepository->save(
                        entity: $order->setData(
                            key: 'resursbank_mapi_payment_id',
                            value: $result[0]->id
                        )
                    );
                    return $result[0]->id;
                }
            } catch (Throwable $error) {
                $this->log->exception(error: $error);
            }
        }

        return $proceed($order);
    }

    /**
     * Intercept calls to getOrderFromPaymentId to handle legacy orders.
     *
     * If the $paymentId parameter is a UUID this method attempts to check if
     * the corresponding order is a legacy order and if so passes the order
     * reference/increment ID instead of the UUID along since the transaction
     * will have used that value instead of the UUID.
     *
     * @param Subject $subject
     * @param callable $proceed
     * @param string $paymentId
     * @return OrderInterface|null
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @noinspection PhpUnusedParameterInspection
     */
    public function aroundGetOrderFromPaymentId(
        Subject $subject,
        callable $proceed,
        string $paymentId
    ): ?OrderInterface {
        try {
            $uuid = $this->stringValidation->isUuid(value: $paymentId);
        } catch (IllegalValueException) {
            $uuid = false;
        }

        if ($uuid) {
            $order = $this->orderInterface->loadByAttribute(
                attribute: 'resursbank_mapi_payment_id',
                value: $paymentId
            );

            try {
                if ($order instanceof OrderInterface &&
                    $order->getData(key: 'resursbank_mapi_payment_id')
                        === $paymentId &&
                    $this->configHelper->isActive(
                        scopeCode: $order->getStoreId()
                    )
                ) {
                    return $order;
                }
            } catch (Throwable $error) {
                $this->log->exception(error: $error);
            }

            try {
                if ($this->configHelper->isActive(scopeCode: $order->getStoreId())) {
                    $mapiOrder = Repository::get(paymentId: $paymentId);

                    $order = $this->orderInterface->loadByIncrementId(
                        incrementId: $mapiOrder->order->orderReference
                    );

                    if ($order instanceof OrderInterface &&
                        $order->getIncrementId()
                        === $mapiOrder->order->orderReference) {
                        return $order;
                    }
                }
            } catch (Throwable $error) {
                $this->log->exception(error: $error);
            }
        }

        return $proceed($paymentId);
    }
}
