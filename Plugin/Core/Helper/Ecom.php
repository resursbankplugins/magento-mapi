<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Mapi\Plugin\Core\Helper;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\ScopeInterface;
use Resursbank\Core\Helper\Ecom as Subject;
use Resursbank\Core\Helper\Scope;
use Resursbank\Core\Helper\Version;
use Resursbank\Ecom\Lib\Api\Environment;
use Resursbank\Ecom\Lib\Api\Scope as EcomScope;
use Resursbank\Mapi\Helper\Config;
use Resursbank\Mapi\Helper\Log;
use Throwable;

/**
 * Overwrite scope applied in communication with API.
 */
class Ecom
{
    /**
     * @param Config $config
     * @param Log $log
     * @param Scope $scope
     * @param Version $version
     */
    public function __construct(
        private readonly Config $config,
        private readonly Log $log,
        private readonly Scope $scope,
        private readonly Version $version
    ) {
    }

    /**
     * Find out if we can connect to MAPI.
     *
     * @param Subject $subject
     * @param bool $result
     * @param string|null $scopeCode
     * @param string $scopeType
     * @return bool
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @noinspection PhpUnusedParameterInspection
     */
    public function afterCanConnect(
        Subject $subject,
        bool $result,
        ?string $scopeCode = null,
        string $scopeType = ScopeInterface::SCOPE_STORES,
    ): bool {
        return $result || $this->config->isActive(
            scopeCode: $scopeCode,
            scopeType: $scopeType
        );
    }

    /**
     * Overwrite API scope.
     *
     * @param Subject $subject
     * @param EcomScope $result
     * @param Environment $environment
     * @return EcomScope
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @noinspection PhpUnusedParameterInspection
     */
    public function afterGetScope(
        Subject $subject,
        EcomScope $result,
        Environment $environment
    ): EcomScope {
        try {
            if ($this->config->isActive(
                scopeCode: $this->scope->getId(),
                scopeType: $this->scope->getType()
            )
            ) {
                $result = EcomScope::MERCHANT_API;
            }
        } catch (Throwable $error) {
            $this->log->exception(error: $error);
        }

        return $result;
    }

    /**
     * Append our module's name to the user-agent string.
     *
     * @param Subject $subject
     * @param string $result
     * @return string
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterGetUserAgent(
        Subject $subject,
        string $result
    ): string {
        if ($this->config->isActive(
            scopeCode: $this->scope->getId(),
            scopeType: $this->scope->getType()
        )) {
            return
                $result . ' Resursbank_Mapi ' .
                $this->version->getComposerVersion(
                    module: 'Resursbank_Mapi'
                ) . ' |';
        }

        return $result;
    }
}
