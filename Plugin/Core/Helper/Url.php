<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Mapi\Plugin\Core\Helper;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Resursbank\Core\Helper\Url as Subject;
use Resursbank\Core\Helper\Scope;
use Resursbank\Mapi\Helper\Config;

/**
 * Appends '/#payment' to the URL we redirect clients to after the cart has been
 * rebuilt (failure page -> rebuild cart -> redirect URL). This ensures clients
 * land on step two of the checkout process.
 */
class Url
{
    /**
     * @param Scope $scope
     * @param Config $config
     */
    public function __construct(
        private readonly Scope $scope,
        private readonly Config $config
    ) {
    }

    /**
     * Intercept calls to the getCheckoutRebuildRedirectUrl method.
     *
     * @param Subject $subject
     * @param string $result
     * @return string
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @noinspection PhpUnusedParameterInspection
     */
    public function afterGetCheckoutRebuildRedirectUrl(
        Subject $subject,
        string $result
    ): string {
        if ($this->config->isActive(scopeCode: $this->scope->getId())) {
            return $result .
                (!str_ends_with(haystack: $result, needle: '/') ? '/' : '') .
                '#payment';
        }

        return $result;
    }
}
