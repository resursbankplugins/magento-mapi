<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Mapi\Plugin\Core\Helper;

use Magento\Store\Model\ScopeInterface;
use Resursbank\Mapi\Helper\Config as ConfigHelper;
use Resursbank\Core\Helper\Config as Subject;

/**
 * Interceptor for the Core Config helper.
 */
class Config
{
    /**
     * @param ConfigHelper $config
     */
    public function __construct(
        private readonly ConfigHelper $config
    ) {
    }

    /**
     * Use MAPI getClientId if flow is active.
     *
     * @param Subject $subject
     * @param callable $proceed
     * @param string|null $scopeCode
     * @param string $scopeType
     * @return string
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @noinspection PhpUnusedParameterInspection
     */
    public function aroundGetClientId(
        Subject $subject,
        callable $proceed,
        ?string $scopeCode,
        string $scopeType = ScopeInterface::SCOPE_STORES
    ): string {
        if ($this->config->isActive(scopeCode: $scopeCode, scopeType: $scopeType)) {
            return $this->config->getClientId(
                scopeCode: $scopeCode,
                scopeType: $scopeType
            );
        }

        return $proceed($scopeCode, $scopeType);
    }

    /**
     * Use MAPI getClientSecret if flow is active.
     *
     * @param Subject $subject
     * @param callable $proceed
     * @param string|null $scopeCode
     * @param string $scopeType
     * @param int|null $environment
     * @return string
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @noinspection PhpUnusedParameterInspection
     */
    public function aroundGetClientSecret(
        Subject $subject,
        callable $proceed,
        ?string $scopeCode,
        string $scopeType = ScopeInterface::SCOPE_STORES,
        ?int $environment = null
    ): string {
        if ($this->config->isActive(scopeCode: $scopeCode, scopeType: $scopeType)) {
            return $this->config->getClientSecret(
                scopeCode: $scopeCode,
                scopeType: $scopeType,
                environment: $environment
            );
        }

        return $proceed($scopeCode, $scopeType, $environment);
    }
}
