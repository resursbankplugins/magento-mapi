<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Mapi\Plugin\Core\Block\Adminhtml\System\Config;

use Resursbank\Core\Block\Adminhtml\System\Config\GetStores as Subject;

/**
 * Interceptor for Core GetStores widget block.
 */
class GetStores
{
    /**
     * Append MAPI get stores controller URL to getUrls output.
     *
     * @param Subject $subject
     * @param array $result
     * @return array
     */
    public function afterGetUrls(
        Subject $subject,
        array $result
    ): array {
        $result['mapi'] = $subject->url->getAdminUrl(
            path: 'resursbank_mapi/data/stores/form_key/' .
            $subject->getFormKey()
        );

        return $result;
    }
}
