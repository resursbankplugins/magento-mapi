<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Mapi\Plugin\Core\Block\Adminhtml\System\Config;

use Resursbank\Core\Block\Adminhtml\System\Config\SupportInfo as Subject;
use Resursbank\Mapi\Helper\Log;
use Magento\Framework\Module\PackageInfo;
use Throwable;

/**
 * Interceptor for Core SupportInfo widget block.
 */
class SupportInfo
{
    /**
     * @param PackageInfo $packageInfo
     * @param Log $log
     */
    public function __construct(
        private readonly PackageInfo $packageInfo,
        private readonly Log $log
    ) {
    }

    /**
     * Append this module's name and version to getVersion output.
     *
     * @param Subject $subject
     * @param string $result
     * @return string
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @noinspection PhpUnusedParameterInspection
     */
    public function afterGetVersion(
        Subject $subject,
        string $result
    ): string {
        try {
            return $result . '<br />Resursbank_Mapi: ' .
                $this->packageInfo->getVersion(moduleName: 'Resursbank_Mapi');
        } catch (Throwable $error) {
            $this->log->exception(error: $error);
        }

        return $result;
    }
}
