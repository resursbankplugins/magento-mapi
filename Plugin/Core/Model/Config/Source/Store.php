<?php

declare(strict_types=1);

namespace Resursbank\Mapi\Plugin\Core\Model\Config\Source;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Resursbank\Core\Helper\Scope;
use Resursbank\Core\Model\Config\Source\Store as Subject;
use Resursbank\Mapi\Helper\Config;
use Resursbank\Mapi\Helper\Log;
use Resursbank\Ecom\Module\Store\Repository as StoreRepository;
use Throwable;

/**
 * Interceptor for store list.
 */
class Store
{
    /**
     * @param Config $config
     * @param Scope $scope
     * @param Log $log
     */
    public function __construct(
        private readonly Config $config,
        private readonly Scope $scope,
        private readonly Log $log
    ) {
    }

    /**
     * Insert MAPI stores if flow is currently active.
     *
     * @param Subject $subject
     * @param array $result
     * @return array
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @noinspection PhpUnusedParameterInspection
     */
    public function afterToArray(
        Subject $subject,
        array $result
    ): array {
        if (!$this->config->isActive(
            scopeCode: $this->scope->getId(),
            scopeType: $this->scope->getType()
        )) {
            return $result;
        }

        try {
            $stores = StoreRepository::getStores();

            /** @var \Resursbank\Ecom\Lib\Model\Store\Store $store */
            foreach ($stores as $store) {
                $result[$store->id] = $store->name;
            }
        } catch (Throwable $error) {
            $this->log->exception(error: $error);
        }

        return $result;
    }
}
