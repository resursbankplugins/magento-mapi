<?php /** @noinspection PhpMultipleClassDeclarationsInspection */
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Mapi\Plugin\Core\Gateway\Command;

use JsonException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\PaymentException;
use Magento\Framework\Phrase;
use Magento\Payment\Gateway\Command\ResultInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Model\Order\Payment;
use ReflectionException;
use Resursbank\Core\Gateway\Command\Authorize as Subject;
use Resursbank\Core\Helper\PaymentMethods\Ecom as PaymentMethods;
use Resursbank\Core\Helper\Url;
use Resursbank\Ecom\Exception\ApiException;
use Resursbank\Ecom\Exception\AttributeCombinationException;
use Resursbank\Ecom\Exception\AuthException;
use Resursbank\Ecom\Exception\ConfigException;
use Resursbank\Ecom\Exception\CurlException;
use Resursbank\Ecom\Exception\Validation\EmptyValueException;
use Resursbank\Ecom\Exception\Validation\IllegalTypeException;
use Resursbank\Ecom\Exception\Validation\IllegalValueException;
use Resursbank\Ecom\Exception\Validation\MissingKeyException;
use Resursbank\Ecom\Exception\Validation\NotJsonEncodedException;
use Resursbank\Ecom\Exception\ValidationException;
use Resursbank\Ecom\Lib\Model\Address;
use Resursbank\Ecom\Lib\Model\Payment\Customer;
use Resursbank\Ecom\Lib\Order\CountryCode;
use Resursbank\Ecom\Module\Customer\Repository as CustomerRepository;
use Resursbank\Ecom\Lib\Model\Payment\CreatePaymentRequest\Options;
use Resursbank\Ecom\Lib\Model\Payment\CreatePaymentRequest\Options\Callback;
use Resursbank\Ecom\Lib\Model\Payment\CreatePaymentRequest\Options\Callbacks;
use Resursbank\Ecom\Lib\Model\Payment\CreatePaymentRequest\Options\ParticipantRedirectionUrls;
use Resursbank\Ecom\Lib\Model\Payment\CreatePaymentRequest\Options\RedirectionUrls;
use Resursbank\Ecom\Module\Payment\Repository;
use Resursbank\Mapi\Helper\Config;
use Resursbank\Mapi\Helper\Log;
use Resursbank\Mapi\Helper\OrderLine;
use Magento\Framework\App\ProductMetadataInterface;
use Resursbank\Core\Helper\Version;
use Resursbank\Mapi\Helper\Session;
use Resursbank\Ordermanagement\Helper\Ecom\Callback as EcomCallbackHelper;
use Throwable;

/**
 * Create payment at Resurs Bank when order is placed.
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.CookieAndSessionMisuse)
 */
class Authorize
{
    /**
     * @param Session $session
     * @param Config $config
     * @param Log $log
     * @param OrderLine $orderLine
     * @param Url $url
     * @param PaymentMethods $paymentMethods
     * @param EcomCallbackHelper $ecomCallbackHelper
     * @param ProductMetadataInterface $productMetadata
     * @param Version $version
     */
    public function __construct(
        private readonly Session $session,
        private readonly Config $config,
        private readonly Log $log,
        private readonly OrderLine $orderLine,
        private readonly Url $url,
        private readonly PaymentMethods $paymentMethods,
        private readonly EcomCallbackHelper $ecomCallbackHelper,
        private readonly ProductMetadataInterface $productMetadata,
        private readonly Version $version
    ) {
    }

    /**
     * Create payment at Resurs Bank.
     *
     * @param Subject $subject
     * @param callable $proceed
     * @param array $commandSubject
     * @return ResultInterface|null
     * @throws PaymentException
     * @throws JsonException
     * @throws ReflectionException
     * @throws ApiException
     * @throws AuthException
     * @throws ConfigException
     * @throws CurlException
     * @throws ValidationException
     * @throws EmptyValueException
     * @throws IllegalTypeException
     * @throws IllegalValueException
     * @throws MissingKeyException
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function aroundExecute(
        Subject $subject,
        callable $proceed,
        array $commandSubject
    ): ?ResultInterface {
        $payment = $subject->getPaymentFromCommandSubject(commandSubject: $commandSubject);
        $order = $payment->getOrder();
        $storeCode = $order->getStore()->getCode();

        if (!$this->config->isActive(scopeCode: $storeCode)) {
            return $proceed($commandSubject);
        }

        try {
            $this->createPayment(order: $order, payment: $payment);
        } catch (PaymentException $error) {
            throw $error;
        } catch (Throwable $error) {
            $this->log->exception(error: $error);

            throw new PaymentException(
                phrase: __('rb-failed-placing-order-try-again')
            );
        }

        return null;
    }

    /**
     * Create payment session at Resurs Bank, or throw exception if failed.
     *
     * @param OrderInterface $order
     * @param Payment $payment
     * @param bool $skipPhone
     * @throws ApiException
     * @throws AttributeCombinationException
     * @throws AuthException
     * @throws ConfigException
     * @throws CurlException
     * @throws EmptyValueException
     * @throws IllegalTypeException
     * @throws IllegalValueException
     * @throws JsonException
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @throws PaymentException
     * @throws ReflectionException
     * @throws ValidationException
     * @throws NotJsonEncodedException
     */
    private function createPayment(
        OrderInterface $order,
        Payment $payment,
        bool $skipPhone = false
    ): void {
        try {
            $session = Repository::create(
                paymentMethodId: $this->paymentMethods->getUuidFromCode(
                    $payment->getMethodInstance()->getCode()
                ),
                orderLines: $this->orderLine->convertOrder(order: $order),
                orderReference: $order->getIncrementId(),
                customer: $this->getCustomer(order: $order, skipPhone: $skipPhone),
                metadata: Repository::getIntegrationInfoMetadata(
                    platform: 'Magento',
                    platformVersion: $this->productMetadata->getVersion(),
                    pluginVersion: $this->version->getComposerVersion(
                        module: 'Resursbank_Mapi'
                    )
                ),
                options: $this->getOptions(order: $order)
            );

            $payment
                ->setTransactionId(transactionId: $session->id)
                ->setIsTransactionClosed(isClosed: false);

            $this->session->setPaymentSigningUrl(
                url: $session->taskRedirectionUrls?->customerUrl ??
                $this->url->getSuccessUrl(quoteId: (int)$order->getQuoteId())
            );

            $order->setData(
                key: 'resursbank_mapi_payment_id',
                value: $session->id
            );

            $order->setData(
                key: 'resursbank_flow',
                value: Config::API_FLOW_OPTION
            );
        } catch (ValidationException $error) {
            if ($error->friendlyMessage !== null) {
                throw new PaymentException(
                    phrase: new Phrase(text: $error->friendlyMessage)
                );
            }

            throw $error;
        } catch (CurlException $error) {
            if ($this->isMobilePhoneInvalid(error: $error)) {
                $this->createPayment(
                    order: $order,
                    payment: $payment,
                    skipPhone: true
                );

                return;
            }

            throw $error;
        }
    }

    /**
     * Confirm if the mobile phone number is invalid.
     *
     * @param CurlException $error
     * @return bool
     */
    private function isMobilePhoneInvalid(CurlException $error): bool
    {
        return (
            $error->httpCode === 400 &&
            str_contains(
                haystack: $error->body,
                needle: 'customer.mobilePhone'
            )
        );
    }

    /**
     * Resolve customer data for payment creation.
     *
     * @param OrderInterface $order
     * @param bool $skipPhone
     * @return Customer
     * @throws AttributeCombinationException
     * @throws ConfigException
     * @throws IllegalValueException
     * @throws JsonException
     * @throws ReflectionException
     */
    private function getCustomer(
        OrderInterface $order,
        bool $skipPhone = false
    ): Customer {
        $ssn = CustomerRepository::getSsnData();

        $address = $order->getShippingAddress();
        $fullName = $address->getCompany() !== '' ?
            $address->getCompany() :
            $address->getFirstname() . ' ' . $address->getLastname();

        $contactPerson = $address->getCompany() !== '' ?
            $address->getFirstname() . ' ' . $address->getLastname() :
            '';

        return new Customer(
            deliveryAddress: new Address(
                addressRow1: $address->getStreetLine(number: 1),
                postalArea: $address->getCity(),
                postalCode: $address->getPostcode(),
                countryCode: CountryCode::from(
                    value: $address->getCountryId()
                ),
                fullName: $fullName,
                firstName: $address->getFirstname(),
                lastName: $address->getLastname(),
                addressRow2: $address->getStreetLine(number: 2),
            ),
            customerType: $ssn?->customerType,
            contactPerson: $contactPerson,
            email: $address->getEmail(),
            governmentId: $ssn?->govId,
            mobilePhone: $skipPhone ? null : $this->getTelephone(order: $order),
        );
    }

    /**
     * Resolve telephone number for payment creation.
     *
     * If the telephone number is different between billing and shipping address,
     * and the billing telephone number is not empty, we will use the billing
     * telephone number, otherwise we will use the shipping telephone number.
     *
     * @param OrderInterface $order
     * @return string|null
     */
    private function getTelephone(
        OrderInterface $order
    ): ?string {
        $shipping = $order->getShippingAddress();
        $billing = $order->getBillingAddress();

        return $billing->getTelephone() !== $shipping->getTelephone() && $billing->getTelephone() !== '' ?
            $billing->getTelephone() :
            $shipping->getTelephone();
    }

    /**
     * Get options for payment creation process.
     *
     * @param OrderInterface $order
     * @return Options
     * @throws AttributeCombinationException
     * @throws IllegalValueException
     * @throws JsonException
     * @throws ReflectionException
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    private function getOptions(OrderInterface $order): Options
    {
        $ttl = $this->config->getMagentoCronCleanupTtl(
            scopeCode: $order->getStore()->getCode()
        );

        return new Options(
            initiatedOnCustomersDevice: true,
            handleManualInspection: false,
            handleFrozenPayments: true,
            automaticCapture: false,
            redirectionUrls: new RedirectionUrls(
                customer: new ParticipantRedirectionUrls(
                    failUrl: $this->url->getFailureUrl(
                        quoteId: (int) $order->getQuoteId()
                    ),
                    successUrl: $this->url->getSuccessUrl(
                        quoteId: (int) $order->getQuoteId()
                    )
                ),
                coApplicant: null,
                merchant: null
            ),
            callbacks: new Callbacks(
                authorization: new Callback(
                    url: $this->ecomCallbackHelper->getUrl(type: 'authorization')
                ),
                management: null
            ),
            timeToLiveInMinutes: (!empty($ttl) && $ttl > 0) ? (int)$ttl : null,
        );
    }
}
