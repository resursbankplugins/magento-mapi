<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

/** @noinspection PhpMultipleClassDeclarationsInspection */

declare(strict_types=1);

namespace Resursbank\Mapi\Plugin\Ordermanagement\Gateway\Command;

use JsonException;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\PaymentException;
use Magento\Payment\Gateway\Command\ResultInterface;
use Magento\Sales\Api\Data\OrderInterface;
use ReflectionException;
use Resursbank\Core\Helper\Ecom;
use Resursbank\Core\Helper\Order as OrderHelper;
use Resursbank\Ecom\Exception\ApiException;
use Resursbank\Ecom\Exception\AuthException;
use Resursbank\Ecom\Exception\ConfigException;
use Resursbank\Ecom\Exception\CurlException;
use Resursbank\Ecom\Exception\Validation\EmptyValueException;
use Resursbank\Ecom\Exception\Validation\IllegalTypeException;
use Resursbank\Ecom\Exception\Validation\IllegalValueException;
use Resursbank\Ecom\Exception\ValidationException;
use Resursbank\Ecom\Lib\Utilities\Strings;
use Resursbank\Ecom\Module\Payment\Enum\ActionType;
use Resursbank\Ecom\Module\Payment\Repository;
use Resursbank\Mapi\Helper\Gateway;
use Resursbank\Mapi\Helper\Log;
use Resursbank\Mapi\Helper\OrderLine as OrderLineHelper;
use Resursbank\Ordermanagement\Gateway\Command\Cancel as Subject;
use Throwable;

/**
 * Handles cancel of MAPI orders.
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Cancel
{
    /**
     * @param OrderHelper $orderHelper
     * @param OrderLineHelper $orderLineHelper
     * @param Log $log
     * @param Gateway $gatewayHelper
     * @param Ecom $ecom
     */
    public function __construct(
        private readonly OrderHelper $orderHelper,
        private readonly OrderLineHelper $orderLineHelper,
        private readonly Log $log,
        private readonly Gateway $gatewayHelper,
        private readonly Ecom $ecom
    ) {
    }

    /**
     * Entry point for the plugin's handling of order cancel.
     *
     * @param Subject $subject
     * @param callable $proceed
     * @param array $commandSubject
     * @return ResultInterface|null
     * @throws PaymentException
     * @throws AlreadyExistsException
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function aroundExecute(
        Subject $subject,
        callable $proceed,
        array $commandSubject
    ): ?ResultInterface {
        try {
            $order = $subject->getOrder(
                commandSubject: $commandSubject,
                log: $this->log
            );

            $this->ecom->connectAftershop(entity: $order);

            if (!$this->gatewayHelper->isEnabled(entity: $order)) {
                return $proceed($commandSubject);
            }

            if (!$this->paymentIsRejected(order: $order->getOrder()) &&
                !$this->cancel(order: $order->getOrder())
            ) {
                throw new ApiException(
                    message: 'An error occurred while canceling the payment.'
                );
            }
        } catch (Throwable $error) {
            $this->log->exception(error: $error);
            $this->orderHelper->throwGatewayException(
                type: ActionType::CANCEL,
                error: $error
            );
        }

        return null;
    }

    /**
     * Check if payment is rejected.
     *
     * @param OrderInterface $order
     * @return bool
     */
    private function paymentIsRejected(OrderInterface $order): bool
    {
        try {
            $paymentId = $this->orderHelper->getPaymentId(order: $order);
            $payment = Repository::get(paymentId: $paymentId);
            if ($payment->isRejected()) {
                return true;
            }
        } catch (Throwable $error) {
            $this->log->error(text: 'Failed to verify payment status');
            $this->log->exception(error: $error);
        }

        return false;
    }

    /**
     * Perform actual cancel of payment.
     *
     * @param OrderInterface $order
     * @return bool True if cancel action has been performed
     * @throws ApiException
     * @throws AuthException
     * @throws ConfigException
     * @throws CurlException
     * @throws EmptyValueException
     * @throws IllegalTypeException
     * @throws IllegalValueException
     * @throws JsonException
     * @throws ReflectionException
     * @throws ValidationException
     * @throws Throwable
     */
    private function cancel(OrderInterface $order): bool
    {
        $paymentId = $this->orderHelper->getPaymentId(order: $order);

        // If we were unable to fetch a UUID payment ID we do nothing.
        if (!Strings::isUuid(value: $paymentId)) {
            return false;
        }

        $payment = Repository::get(paymentId: $paymentId);

        if ($payment->canCancel()) {
            $result = Repository::cancel(
                paymentId: $paymentId,
                orderLines: $this->orderLineHelper->convertOrder(order: $order)
            );

            if ($result->isCancelled()) {
                return true;
            }
        }

        return false;
    }
}
