<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

/** @noinspection PhpMultipleClassDeclarationsInspection */

declare(strict_types=1);

namespace Resursbank\Mapi\Plugin\Ordermanagement\Gateway\Command;

use JsonException;
use Magento\Framework\Exception\PaymentException;
use Magento\Payment\Gateway\Command\ResultInterface;
use Magento\Sales\Model\Order\Invoice;
use ReflectionException;
use Resursbank\Core\Gateway\Command;
use Resursbank\Core\Helper\Ecom;
use Resursbank\Core\Helper\Order as OrderHelper;
use Resursbank\Ecom\Exception\ApiException;
use Resursbank\Ecom\Exception\AttributeCombinationException;
use Resursbank\Ecom\Exception\AuthException;
use Resursbank\Ecom\Exception\ConfigException;
use Resursbank\Ecom\Exception\CurlException;
use Resursbank\Ecom\Exception\Validation\EmptyValueException;
use Resursbank\Ecom\Exception\Validation\IllegalTypeException;
use Resursbank\Ecom\Exception\Validation\IllegalValueException;
use Resursbank\Ecom\Exception\Validation\NotJsonEncodedException;
use Resursbank\Ecom\Exception\ValidationException;
use Resursbank\Ecom\Module\Payment\Enum\ActionType;
use Resursbank\Ecom\Module\Payment\Repository;
use Resursbank\Mapi\Helper\Gateway;
use Resursbank\Mapi\Helper\Log;
use Resursbank\Mapi\Helper\OrderLine as OrderLineHelper;
use Resursbank\Ordermanagement\Gateway\Command\Capture as Subject;
use Throwable;

/**
 * Handles capture of MAPI orders.
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Capture extends Command
{
    /**
     * @param Ecom $ecom
     * @param Log $log
     * @param OrderHelper $orderHelper
     * @param OrderLineHelper $orderLineHelper
     * @param Gateway $gatewayHelper
     */
    public function __construct(
        private readonly Ecom $ecom,
        private readonly Log $log,
        private readonly OrderHelper $orderHelper,
        private readonly OrderLineHelper $orderLineHelper,
        private readonly Gateway $gatewayHelper
    ) {
    }

    /**
     * Entry point for the plugin's handling of order capture.
     *
     * @param Subject $subject
     * @param callable $proceed
     * @param array $commandSubject
     * @return ResultInterface|null
     * @throws PaymentException
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function aroundExecute(
        Subject $subject,
        callable $proceed,
        array $commandSubject
    ): ?ResultInterface {
        try {
            $invoice = $subject->getInvoice()->getInvoice();

            $this->ecom->connectAftershop(entity: $invoice);

            if (!$invoice instanceof Invoice) {
                throw new PaymentException(
                    phrase: __('rb-unable-to-read-invoice-from-command-subject')
                );
            }

            if (!$this->gatewayHelper->isEnabled(entity: $invoice)) {
                return $proceed($commandSubject);
            }

            $this->capture(invoice: $invoice);
        } catch (Throwable $error) {
            $this->log->exception(error: $error);
            $this->orderHelper->throwGatewayException(
                type: ActionType::CAPTURE,
                error: $error
            );
        }

        return null;
    }

    /**
     * Perform actual capture of payment.
     *
     * @param Invoice $invoice
     * @return void
     * @throws ApiException
     * @throws AuthException
     * @throws ConfigException
     * @throws CurlException
     * @throws EmptyValueException
     * @throws IllegalTypeException
     * @throws IllegalValueException
     * @throws JsonException
     * @throws PaymentException
     * @throws ReflectionException
     * @throws ValidationException
     * @throws AttributeCombinationException
     * @throws NotJsonEncodedException
     * @throws AttributeCombinationException
     */
    private function capture(Invoice $invoice): void
    {
        $order = $invoice->getOrder();
        $paymentId = $this->orderHelper->getPaymentId(order: $order);

        if ($paymentId === '') {
            throw new PaymentException(
                phrase: __('rb-missing-payment-id')
            );
        }

        Repository::capture(
            paymentId: $paymentId,
            orderLines: $this->orderLineHelper->convertInvoice(
                invoice: $invoice
            )
        );
    }
}
