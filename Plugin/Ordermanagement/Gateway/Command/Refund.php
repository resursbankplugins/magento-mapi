<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

/** @noinspection PhpMultipleClassDeclarationsInspection */

declare(strict_types=1);

namespace Resursbank\Mapi\Plugin\Ordermanagement\Gateway\Command;

use JsonException;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\PaymentException;
use Magento\Payment\Gateway\Command\ResultInterface;
use Magento\Sales\Model\Order\Creditmemo;
use ReflectionException;
use Resursbank\Core\Gateway\Command;
use Resursbank\Core\Helper\Ecom;
use Resursbank\Core\Helper\Order;
use Resursbank\Ecom\Exception\ApiException;
use Resursbank\Ecom\Exception\AuthException;
use Resursbank\Ecom\Exception\ConfigException;
use Resursbank\Ecom\Exception\CurlException;
use Resursbank\Ecom\Exception\Validation\EmptyValueException;
use Resursbank\Ecom\Exception\Validation\IllegalTypeException;
use Resursbank\Ecom\Exception\Validation\IllegalValueException;
use Resursbank\Ecom\Exception\ValidationException;
use Resursbank\Ecom\Module\Payment\Enum\ActionType;
use Resursbank\Ecom\Module\Payment\Repository;
use Resursbank\Mapi\Helper\Gateway;
use Resursbank\Mapi\Helper\Log;
use Resursbank\Mapi\Helper\OrderLine;
use Resursbank\Ordermanagement\Gateway\Command\Refund as Subject;
use Throwable;

/**
 * Handles refund of MAPI orders.
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Refund extends Command
{
    /**
     * @param Order $orderHelper
     * @param OrderLine $orderLineHelper
     * @param Log $log
     * @param Gateway $gatewayHelper
     * @param Ecom $ecom
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        private readonly Order $orderHelper,
        private readonly OrderLine $orderLineHelper,
        private readonly Log $log,
        private readonly Gateway $gatewayHelper,
        private readonly Ecom $ecom
    ) {
    }

    /**
     * Entry point for the plugin's handling of order refund.
     *
     * @param Subject $subject
     * @param callable $proceed
     * @param array $commandSubject
     * @return ResultInterface|null
     * @throws AlreadyExistsException
     * @throws PaymentException
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function aroundExecute(
        Subject $subject,
        callable $proceed,
        array $commandSubject
    ): ?ResultInterface {
        try {
            $creditmemo = $subject->getCreditmemo(
                payment: $this->getPayment(
                    commandSubject: $commandSubject,
                    log: $this->log
                )
            );

            $this->ecom->connectAftershop(entity: $creditmemo);

            if (!$this->gatewayHelper->isEnabled(entity: $creditmemo)) {
                return $proceed($commandSubject);
            }

            $this->refund(creditmemo: $creditmemo);
        } catch (Throwable $error) {
            $this->log->exception(error: $error);
            $this->orderHelper->throwGatewayException(
                type: ActionType::REFUND,
                error: $error
            );
        }

        return null;
    }

    /**
     * Perform actual refund of payment.
     *
     * @param Creditmemo $creditmemo
     * @return void
     * @throws ApiException
     * @throws AuthException
     * @throws ConfigException
     * @throws CurlException
     * @throws EmptyValueException
     * @throws IllegalTypeException
     * @throws IllegalValueException
     * @throws JsonException
     * @throws PaymentException
     * @throws ReflectionException
     * @throws ValidationException
     * @throws Throwable
     */
    private function refund(Creditmemo $creditmemo): void
    {
        $order = $creditmemo->getOrder();
        $paymentId = $this->orderHelper->getPaymentId(order: $order);

        if ($paymentId === '') {
            throw new PaymentException(phrase: __('rb-missing-payment-id'));
        }

        Repository::refund(
            paymentId: $paymentId,
            orderLines: $this->orderLineHelper->convertCreditmemo(
                creditmemo: $creditmemo
            )
        );
    }
}
