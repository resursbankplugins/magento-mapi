<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Mapi\Plugin\Ordermanagement\Helper;

use Magento\Store\Model\ScopeInterface;
use Resursbank\Mapi\Helper\Config as ConfigHelper;
use Resursbank\Ordermanagement\Helper\Config as Subject;

/**
 * Overrides for the Ordermanagement Config helper.
 */
class Config
{
    /**
     * @param ConfigHelper $config
     */
    public function __construct(
        private readonly ConfigHelper $config
    ) {
    }

    /**
     * Always return
     *
     * @param Subject $subject
     * @param bool $result
     * @param string $scopeCode
     * @param string $scopeType
     * @return bool
     */
    public function afterIsAutoInvoiceEnabled(
        Subject $subject,
        bool $result,
        string $scopeCode,
        string $scopeType = ScopeInterface::SCOPE_STORES
    ): bool {
        if ($this->config->isActive(
            scopeCode: $scopeCode,
            scopeType: $scopeType
        )) {
            return true;
        }

        return $result;
    }
}
