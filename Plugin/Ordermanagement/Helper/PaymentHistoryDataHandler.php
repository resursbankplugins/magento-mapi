<?php /** @noinspection PhpMultipleClassDeclarationsInspection */
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Mapi\Plugin\Ordermanagement\Helper;

use JsonException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Api\Data\OrderInterface;
use ReflectionException;
use Resursbank\Ecom\Exception\ApiException;
use Resursbank\Ecom\Exception\AttributeCombinationException;
use Resursbank\Ecom\Exception\AuthException;
use Resursbank\Ecom\Exception\ConfigException;
use Resursbank\Ecom\Exception\CurlException;
use Resursbank\Ecom\Exception\Validation\EmptyValueException;
use Resursbank\Ecom\Exception\Validation\IllegalTypeException;
use Resursbank\Ecom\Exception\Validation\IllegalValueException;
use Resursbank\Ecom\Exception\Validation\NotJsonEncodedException;
use Resursbank\Ecom\Exception\ValidationException;
use Resursbank\Ecom\Lib\Model\PaymentHistory\Entry;
use Resursbank\Ecom\Lib\Model\Payment;
use Resursbank\Ecom\Module\Payment\Repository;
use Resursbank\Ordermanagement\Helper\PaymentHistoryDataHandler as Subject;
use Resursbank\Mapi\Helper\Config;

/**
 * Payment history data handler plugin.
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class PaymentHistoryDataHandler
{
    /**
     * @param Config $config
     */
    public function __construct(
        private readonly Config $config
    ) {
    }

    /**
     * Update order state/status based on payment status.
     *
     * @param Subject $subject
     * @param mixed $result
     * @param OrderInterface $order
     * @param Entry $entry
     * @return void
     * @throws ApiException
     * @throws AttributeCombinationException
     * @throws AuthException
     * @throws ConfigException
     * @throws CurlException
     * @throws EmptyValueException
     * @throws IllegalTypeException
     * @throws IllegalValueException
     * @throws JsonException
     * @throws NotJsonEncodedException
     * @throws ReflectionException
     * @throws ValidationException
     * @throws LocalizedException
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterSyncOrder(
        Subject $subject,
        mixed $result,
        OrderInterface $order,
        Entry $entry
    ): void {
        if (!$this->config->isActive(scopeCode: $order->getStore()->getCode())) {
            return;
        }

        $payment = $this->getPayment(entry: $entry);

        if ($payment->isCancelled()) {
            $subject->handleCancelledPayment(order: $order);
        } elseif ($payment->isFrozen()) {
            $subject->handleFrozenPayment(order: $order);
        } elseif ($payment->canCapture() || $payment->isCaptured()) {
            $subject->handleProcessingPayment(order: $order);
        }
    }

    /**
     * Resolve payment/checkout instance matching supplied payment id.
     *
     * @param Entry $entry
     * @return Payment
     * @throws ApiException
     * @throws AuthException
     * @throws ConfigException
     * @throws CurlException
     * @throws EmptyValueException
     * @throws IllegalTypeException
     * @throws IllegalValueException
     * @throws JsonException
     * @throws ReflectionException
     * @throws ValidationException
     * @throws AttributeCombinationException
     * @throws NotJsonEncodedException
     * @throws AttributeCombinationException
     */
    private function getPayment(
        Entry $entry
    ): PAyment {
        return Repository::get(paymentId: $entry->paymentId);
    }
}
