<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Mapi\Plugin\Config;

use Magento\Framework\Phrase;
use Resursbank\Core\Model\Config\Source\Flow as Subject;
use Resursbank\Mapi\Helper\Config;

class AddFlowOption
{
    /**
     * Add MAPI to the list of available API flows in Core module.
     *
     * @param Subject $subject
     * @param array $result
     * @return array<string, Phrase>
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @noinspection PhpUnusedParameterInspection
     */
    public function afterToArray(
        Subject $subject,
        array $result
    ): array {
        $result[Config::API_FLOW_OPTION] = __('rb-mapi');

        return $result;
    }
}
