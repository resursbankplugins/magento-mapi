<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Mapi\Helper;

use Magento\Sales\Api\Data\OrderInterface;
use Resursbank\Core\Helper\Order as CoreOrder;

/**
 * Helper functions for order handling.
 */
class Order extends CoreOrder
{
    /**
     * Checks if order was created using the MAPI flow.
     *
     * @param OrderInterface $order
     * @return bool
     */
    public function isMapi(OrderInterface $order): bool
    {
        return $order->getData(key: 'resursbank_flow') === Config::API_FLOW_OPTION;
    }
}
