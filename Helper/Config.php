<?php
/**
 * Copyright Â© Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Mapi\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Store\Model\ScopeInterface;
use Resursbank\Core\Helper\Config as CoreConfig;
use Resursbank\Core\Helper\AbstractConfig;

class Config extends AbstractConfig
{
    /**
     * API flow option appended by this module.
     */
    public const API_FLOW_OPTION = 'mapi';

    /**
     * @param ScopeConfigInterface $reader
     * @param WriterInterface $writer
     * @param Context $context
     * @param CoreConfig $coreConfig
     * @param EncryptorInterface $encryptor
     */
    public function __construct(
        ScopeConfigInterface $reader,
        WriterInterface $writer,
        Context $context,
        private readonly CoreConfig $coreConfig,
        private readonly EncryptorInterface $encryptor
    ) {
        parent::__construct($reader, $writer, $context);
    }

    /**
     * Check if flow is active.
     *
     * @param string|null $scopeCode
     * @param string $scopeType
     * @return bool
     */
    public function isActive(
        ?string $scopeCode = null,
        string $scopeType = ScopeInterface::SCOPE_STORES
    ): bool {
        return $this->coreConfig->getFlow(
            scopeCode: $scopeCode,
            scopeType: $scopeType
        ) === self::API_FLOW_OPTION;
    }

    /**
     * Get configured Client ID (utilised for modern APIs).
     *
     * @param string|null $scopeCode
     * @param string $scopeType
     * @return string
     */
    public function getClientId(
        ?string $scopeCode,
        string $scopeType = ScopeInterface::SCOPE_STORES
    ): string {
        return (string)$this->get(
            group: CoreConfig::API_GROUP,
            key: 'client_id_mapi_' . $this->coreConfig->getEnvironment(
                scopeCode: $scopeCode,
                scopeType: $scopeType
            ),
            scopeCode: $scopeCode,
            scopeType: $scopeType
        );
    }

    /**
     * Get configured API secret (utilised for modern APIs).
     *
     * @param string|null $scopeCode
     * @param string $scopeType
     * @param int|null $environment
     * @return string
     */
    public function getClientSecret(
        ?string $scopeCode,
        string $scopeType = ScopeInterface::SCOPE_STORES,
        ?int $environment = null
    ): string {
        /* When fetching stores we may need to resolve secret for a specified
           environment. See \Resursbank\Core\Controller\Adminhtml\Data\Stores::getRequestData */
        if ($environment === null) {
            $environment = $this->coreConfig->getEnvironment(
                scopeCode: $scopeCode,
                scopeType: $scopeType
            );
        }

        return $this->encryptor->decrypt(
            data: (string)$this->get(
                group: CoreConfig::API_GROUP,
                key: sprintf('client_secret_mapi_%d', $environment),
                scopeCode: $scopeCode,
                scopeType: $scopeType
            )
        );
    }

    /**
     * Fetch the configured Pending payment cleanup TTL.
     *
     * @param string|null $scopeCode
     * @param string $scopeType
     * @return string
     */
    public function getMagentoCronCleanupTtl(
        ?string $scopeCode,
        string $scopeType = ScopeInterface::SCOPE_STORES
    ): string {
        return $this->reader->getValue(
            'sales/orders/delete_pending_after',
            $scopeType,
            $scopeCode
        );
    }
}
