<?php /** @noinspection PhpMultipleClassDeclarationsInspection */
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Mapi\Helper;

use Exception;
use JsonException;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Model\Order\Creditmemo;
use Magento\Sales\Model\Order\Invoice;
use ReflectionException;
use Resursbank\Core\Model\Api\Payment\Item;
use Resursbank\Ecom\Exception\AttributeCombinationException;
use Resursbank\Ecom\Exception\Validation\IllegalTypeException;
use Resursbank\Ecom\Lib\Model\Payment\Order\ActionLog\OrderLine as EcomOrderLine;
use Resursbank\Ecom\Lib\Model\Payment\Order\ActionLog\OrderLineCollection;
use Resursbank\Ecom\Lib\Order\OrderLineType;
use Resursbank\Ordermanagement\Model\Api\Payment\Converter\InvoiceConverter;
use Resursbank\Ordermanagement\Model\Api\Payment\Converter\CreditmemoConverter;
use Resursbank\Ordermanagement\Model\Api\Payment\Converter\OrderConverter;

/**
 * Converts Magento Invoice|Creditmemo|Order to data readable by API.
 *
 * We first convert the original entity to legacy items lines (using the
 * converter classes from the Core module) and then convert these to OrderLine.
 *
 * We do this because there is a lot of complex business logic in the Core
 * converter classes that we don't want to duplicate here (for example to handle
 * discounts, shipping, etc. when there are mixed tax rates in the entity).
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class OrderLine
{
    /**
     * @param InvoiceConverter $invoiceConverter
     * @param CreditmemoConverter $creditmemoConverter
     * @param OrderConverter $orderConverter
     */
    public function __construct(
        private readonly InvoiceConverter $invoiceConverter,
        private readonly CreditmemoConverter $creditmemoConverter,
        private readonly OrderConverter $orderConverter
    ) {
    }

    /**
     * Convert Magento Invoice to API data.
     *
     * @param Invoice $invoice
     * @return OrderLineCollection
     * @throws AttributeCombinationException
     * @throws IllegalTypeException
     * @throws JsonException
     * @throws ReflectionException
     * @throws Exception
     */
    public function convertInvoice(Invoice $invoice): OrderLineCollection
    {
        return new OrderLineCollection(
            data: $this->convertItemLines(
                items: $this->invoiceConverter->convert($invoice)
            )
        );
    }

    /**
     * Convert Magento Creditmemo to API data.
     *
     * @param Creditmemo $creditmemo
     * @return OrderLineCollection
     * @throws AttributeCombinationException
     * @throws IllegalTypeException
     * @throws JsonException
     * @throws ReflectionException
     * @throws Exception
     */
    public function convertCreditmemo(Creditmemo $creditmemo): OrderLineCollection
    {
        return new OrderLineCollection(
            data: $this->convertItemLines(
                items: $this->creditmemoConverter->convert($creditmemo)
            )
        );
    }

    /**
     * Convert Magento Order to API data.
     *
     * @param OrderInterface $order
     * @return OrderLineCollection
     * @throws AttributeCombinationException
     * @throws IllegalTypeException
     * @throws JsonException
     * @throws ReflectionException
     * @throws Exception
     */
    public function convertOrder(OrderInterface $order): OrderLineCollection
    {
        return new OrderLineCollection(
            data: $this->convertItemLines(
                items: $this->orderConverter->convert(entity: $order)
            )
        );
    }

    /**
     * Convert legacy item lines to OrderLine instances.
     *
     * @param array $items
     * @return array
     * @throws JsonException
     * @throws ReflectionException
     * @throws AttributeCombinationException
     */
    private function convertItemLines(array $items): array
    {
        $result = [];

        foreach ($items as $item) {
            /** @var Item $item */
            $result[] = new EcomOrderLine(
                quantity: round($item->getQuantity(), 2),
                quantityUnit: $item->getUnitMeasure(),
                vatRate: $item->getVatPct(),
                totalAmountIncludingVat: round($item->getTotalAmountInclVat(), 2),
                description: $item->getDescription(),
                reference: $item->getArtNo(),
                type: $this->getType($item->getType())
            );
        }

        return $result;
    }

    /**
     * Convert item line type to OrderLineType.
     *
     * @param string $type
     * @return OrderLineType
     */
    private function getType(string $type): OrderLineType
    {
        return match ($type) {
            Item::TYPE_DISCOUNT => OrderLineType::DISCOUNT,
            Item::TYPE_SHIPPING => OrderLineType::SHIPPING,
            default => OrderLineType::NORMAL,
        };
    }
}
