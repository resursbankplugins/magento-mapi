<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Mapi\Helper;

use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Model\Order\Creditmemo;
use Magento\Sales\Model\Order\Invoice;
use Resursbank\Core\Gateway\Data\Order\OrderAdapter;
use Resursbank\Core\Helper\Order as CoreOrderHelper;
use Resursbank\Ordermanagement\Helper\Config as OrdermanagementConfig;
use Throwable;

/**
 * General gateway command functions.
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Gateway
{
    /**
     * @param OrdermanagementConfig $ordermanagementConfig
     * @param Order $orderHelper
     * @param CoreOrderHelper $coreOrderHelper
     * @param Log $log
     * @param Config $config
     */
    public function __construct(
        private readonly OrdermanagementConfig $ordermanagementConfig,
        private readonly Order $orderHelper,
        private readonly CoreOrderHelper $coreOrderHelper,
        private readonly Log $log,
        private readonly Config $config
    ) {
    }

    /**
     * Whether MAPI gateway commands are enabled.
     *
     * Checks if the after shop functionality is enabled and if either the order
     * is a MAPI order or, if it's a legacy order, we're able to fetch it
     * using the MAPI API.
     *
     * @param OrderAdapter|Invoice|Creditmemo|OrderInterface $entity
     * @return bool
     */
    public function isEnabled(
        OrderAdapter|Invoice|Creditmemo|OrderInterface $entity
    ): bool {
        try {
            $order = $entity instanceof OrderInterface ?
                $entity : $entity->getOrder();

            return (
                $this->config->isActive(scopeCode: $order->getStoreId()) &&
                $this->ordermanagementConfig->isAfterShopEnabled(
                    scopeCode: $entity->getStore()->getCode()
                ) &&
                (
                    $this->orderHelper->isMapi(order: $order)
                    || $this->coreOrderHelper->isProcessableLegacy(
                        order: $order
                    )
                )
            );
        } catch (Throwable $error) {
            $this->log->exception(error: $error);
        }

        return false;
    }
}
