/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

/**
 * @typedef {object} Mapi.Checkout.MethodRender
 * @property {string} component
 * @property {string} name
 * @property {string} methodCode
 * @property {string} displayArea
 * @property {object} config
 */

/**
 * @typedef {
 *  Array<Mapi.Checkout.MethodRender>
 * } Mapi.Checkout.MethodRenderList
 */

/**
 * @callback Mapi.Ko.MethodRenderList
 * @param {Mapi.Checkout.MethodRenderList} [value]
 * @return {Mapi.Checkout.MethodRenderList}
 */

define(
    [
        'ko'
    ],

    /**
     * @param ko
     * @return {Mapi.Ko.MethodRenderList}
     */
    function (
        ko
    ) {
        'use strict';

        return ko.observableArray([]);
    }
);
