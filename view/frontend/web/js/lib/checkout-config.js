/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

// phpcs:ignoreFile
define(
    [],
    /**
     * @returns Readonly<Mapi.Lib.CheckoutConfig>
     */
    function () {
        'use strict';

        /**
         * @typedef {object} Mapi.Lib.CheckoutConfig.PaymentMethod
         * @property {string} code
         * @property {string} type
         * @property {string} title
         * @property {number} maxOrderTotal
         * @property {string} specificType
         * @property {["NATURAL"|"LEGAL"]} customerType
         * @property {string} usp
         * @property {string} readMore
         * @property {string} costList
         * @property {string} priceSignageWarning
         */

        /**
         * Includes functions that handle Magento's global frontend object
         * "window.checkoutConfig".
         *
         * @constant
         * @namespace Mapi.Lib.CheckoutConfig
         */
        var EXPORT = {
            /**
             * Returns the form key to make server requests with.
             *
             * @returns {string} An empty string will be returned if the form
             * key couldn't be found.
             */
            getFormKey: function() {
                return window.hasOwnProperty('checkoutConfig') &&
                    typeof window.checkoutConfig.formKey === 'string' ?
                        window.checkoutConfig.formKey :
                        '';
            },

            /**
             * Returns an object containing the data for all the available
             * Resurs Bank payment methods.
             *
             * @returns {Mapi.Lib.CheckoutConfig.PaymentMethod[]}
             */
            getPaymentMethods: function() {
                var config = window.checkoutConfig;

                /**
                 * @type {Mapi.Lib.CheckoutConfig.PaymentMethod[]}
                 */
                var result = [];

                if (config.hasOwnProperty('payment') &&
                    config.payment.hasOwnProperty('resursbank_mapi') &&
                    Array.isArray(config.payment.resursbank_mapi.methods)
                ) {
                    result = config.payment.resursbank_mapi.methods;
                }

                return result;
            },

            /**
             * Returns the data for a Resurs Bank payment method using the
             * provided payment method code.
             *
             * @returns {
             * (Mapi.Lib.CheckoutConfig.PaymentMethod|undefined)
             * } The data for a Resurs Bank payment method. If the code does
             * not point to a Resurs Bank payment method in the
             * "window.checkoutConfig" object, undefined is returned.
             */
            getPaymentMethod: function(code) {
                return EXPORT.getPaymentMethods()
                    .filter(function (method) {
                        return method.code === code;
                    })[0];
            }
        };

        return Object.freeze(EXPORT);
    }
);
