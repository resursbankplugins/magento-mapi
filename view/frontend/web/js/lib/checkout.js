/**
 * Copyright Â© Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

// phpcs:ignoreFile
define(
    [
        'jquery',
        'uiRegistry'
    ],
    /**
     * @param $
     * @param uiRegistry
     * @returns Readonly<Mapi.Lib.Checkout>
     */
    function (
        $,
        uiRegistry
    ) {
        'use strict';

        /**
         * @typedef {object} Mapi.Lib.FetchAddress.Address
         * @property {string} firstName
         * @property {string} lastName
         * @property {string} postalArea
         * @property {string} fullName
         * @property {string} countryCode
         * @property {string} postalCode
         * @property {string} addressRow1
         * @property {string} addressRow2
         */

        /**
         * @typedef {object} Mapi.Lib.Checkout.AddressInputs
         * @property {object} firstname
         * @property {object} lastname
         * @property {object} company
         * @property {object} street0
         * @property {object} street1
         * @property {object} street2
         * @property {object} postcode
         * @property {object} country
         * @property {object} city
         * @property {object} telephone
         */

        /**
         * @constant
         * @namespace Mapi.Lib.Checkout
         */
        var EXPORT = {
            /**
             * Returns the JS-components of the input fields in the mapi
             * checkout.
             *
             * @returns {Mapi.Lib.Checkout.AddressInputs}
             */
            getAddressInputs: function() {
                var path =
                    'checkout.steps.shipping-step.shippingAddress' +
                    '.shipping-address-fieldset';

                return {
                    firstname: uiRegistry.get(path + '.firstname'),
                    lastname: uiRegistry.get(path + '.lastname'),
                    company: uiRegistry.get(path + '.company'),
                    street0: uiRegistry.get(path + '.street.0'),
                    street1: uiRegistry.get(path + '.street.1'),
                    street2: uiRegistry.get(path + '.street.2'),
                    postcode: uiRegistry.get(path + '.postcode'),
                    country: uiRegistry.get(path + '.country_id'),
                    city: uiRegistry.get(path + '.city'),
                    telephone: uiRegistry.get(path + '.telephone')
                };
            },

            /**
             * Applies a fetched shipping address to the shipping address
             * input fields in the mapi checkout.
             *
             * @param {Mapi.Lib.FetchAddress.Address} address
             * @param {String} customerType
             * @return {Readonly<Mapi.Lib.Checkout>}
             */
            applyAddress: function(address, customerType) {
                var inputs = EXPORT.getAddressInputs();

                inputs.firstname.value(address.firstName);
                inputs.lastname.value(address.lastName);
                inputs.city.value(address.postalArea);
                inputs.postcode.value(address.postalCode);
                inputs.street0.value(address.addressRow1);
                inputs.street1.value(address.addressRow2);
                inputs.country.value(address.countryCode);

                if (customerType === 'LEGAL') {
                    inputs.company.value(address.fullName);
                }

                return EXPORT;
            },

            /**
             * Removes an applied shipping address from the input fields of
             * the mapi checkout. The input fields will have their
             * initial values restored.
             *
             * @return {Readonly<Mapi.Lib.Checkout>}
             */
            removeAddress: function() {
                var inputs = EXPORT.getAddressInputs();

                inputs.firstname.reset();
                inputs.lastname.reset();
                inputs.city.reset();
                inputs.postcode.reset();
                inputs.street0.reset();
                inputs.street1.reset();
                inputs.country.reset();
                inputs.company.reset();

                return EXPORT;
            }
        };

        return Object.freeze(EXPORT);
    }
);
