/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */
/* jshint esversion: 6 */

// noinspection JSUnusedLocalSymbols
/**
 * Implementation of GetAddress widget.
 */
define(
    [
        'jquery',
        'uiComponent',
        'Resursbank_Mapi/js/lib/checkout',
        'Resursbank_Mapi/js/lib/checkout-config',
        'Magento_Checkout/js/model/quote'
    ],

    /**
     * @param {jQuery} $
     * @param {Component} Component
     * @param {Mapi.Lib.Checkout} checkoutLib
     * @param {Mapi.Lib.CheckoutConfig} checkoutConfig
     *
     * @param quote
     * @returns {*}
     */
    function (
        $,
        Component,
        checkoutLib,
        checkoutConfig,
        quote
    ) {
        'use strict';

        // Cannot proceed unless widget has loaded.
        if (typeof Resursbank_GetAddress !== 'function') {
            return;
        }

        /**
         * Corrects the position of the widget and displays it.
         *
         * @param {Resursbank_GetAddress} widget
         */
        function correctWidgetPosition(widget) {
            let observer = new MutationObserver(function(mutations) {
                // Wait for the login form element to become available.
                let shippingForm = document.getElementById('co-shipping-form');

                if (!(shippingForm instanceof HTMLElement)) {
                    return;
                }

                const widgetEl = widget.getWidgetElement();

                // Insert the widget after the login form and display it.
                shippingForm.insertAdjacentElement('beforebegin', widgetEl);
                widgetEl.style.display = 'block';

                // Disconnect the observer once the widget has been displayed.
                observer.disconnect();
            });

            // Observe the DOM, waiting for the login form to appear.
            observer.observe(document, { childList: true, subtree: true });
        }

        return Component.extend({
            initialize: function() {
                this._super();

                const widget = new Resursbank_GetAddress({
                    updateAddress: function(data) {
                        checkoutLib.removeAddress();
                        checkoutLib.applyAddress(data, this.getCustomerType());
                    },
                    onComplete: function() {
                        RESURSBANK_GOV_ID = document.getElementById('rb-ga-gov-id').value;
                    }
                });

                widget.setupEventListeners();

                try {
                    // Add additional classes to the fetch button.
                    widget.getFetchBtnElement().classList.add(
                        'primary',
                        'action'
                    );

                    // Correct the position of the widget, and display it.
                    correctWidgetPosition(widget);
                } catch (e) {
                    console.warn(e);
                }
            }
        });
    }
);
