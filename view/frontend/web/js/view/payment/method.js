/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

// phpcs:ignoreFile
/**
 * This component represents a payment method on the billing step of the
 * checkout process.
 */
define(
    [
        'jquery',
        'ko',
        'mage/url',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/view/payment/default',
        'Magento_Checkout/js/action/redirect-on-success',
        'Resursbank_Mapi/js/lib/checkout-config',
        'Resursbank_Mapi/js/lib/credentials',
        'Resursbank_Mapi/js/model/payment/method-render-list'
    ],

    /**
     * @param $
     * @param ko
     * @param url
     * @param Quote
     * @param Component
     * @param redirectOnSuccessAction
     * @param {Mapi.Lib.CheckoutConfig} CheckoutConfigLib
     * @param {Mapi.Lib.Credentials} CredentialsLib
     * @returns {*}
     */
    function (
        $,
        ko,
        url,
        Quote,
        Component,
        redirectOnSuccessAction,
        CheckoutConfigLib,
        CredentialsLib
    ) {
        'use strict';

        /**
         * 1. If a billing address exists on the quote, return that.
         * 2. If the customer is logged in and has a default billing address,
         *   return that.
         * 3. Return the shipping address.
         *
         * @returns {null|object}
         */
        function getAddress() {
            if (Quote.billingAddress()) {
                return Quote.billingAddress();
            }

            if (window.isCustomerLoggedIn &&
                window.customerData &&
                window.customerData.addresses &&
                window.customerData.default_billing &&
                window.customerData.addresses.hasOwnProperty(window.customerData.default_billing)
            ) {
                return window.customerData.addresses[window.customerData.default_billing];
            }

            return Quote.shippingAddress();
        }

        /**
         * Checks whether a payment method has an SSN field.
         *
         * @param {string} code
         * @returns {boolean}
         */
        function hasSsnField(code) {
            var method = CheckoutConfigLib.getPaymentMethod(code);

            return typeof method !== 'undefined' ?
                method.type !== 'PAYMENT_PROVIDER' :
                false;
        }

        /**
         * Checks whether a payment method is provided by Resurs Bank directly.
         *
         * @param {string} code
         * @returns {boolean}
         */
        function isResursInternalMethod(code) {
            return CheckoutConfigLib.getPaymentMethods().some(
                function(method) {
                    return (
                        method.code === code &&
                        method.type !== 'PAYMENT_PROVIDER'
                    );
                }
            );
        }

        /**
         * Checks whether a payment method is connected to Swish.
         *
         * @param {string} code
         * @returns {boolean}
         */
        function isSwishMethod(code) {
            return CheckoutConfigLib.getPaymentMethods().some(
                function(method) {
                    return (
                        method.code === code &&
                        method.type === 'PAYMENT_PROVIDER' &&
                        method.specificType === 'SWISH'
                    );
                }
            );
        }

        /**
         * Checks whether a payment method is a credit card.
         *
         * @param {string} code
         * @returns {boolean}
         */
        function isCreditCardMethod(code) {
            return CheckoutConfigLib.getPaymentMethods().some(
                function(method) {
                    return (
                        method.code === code &&
                        method.type === 'PAYMENT_PROVIDER' &&
                        (
                            method.specificType === 'DEBIT_CARD' ||
                            method.specificType === 'CREDIT_CARD'
                        )
                    );
                }
            );
        }

        /**
         * Checks whether a payment method is a Trustly payment method.
         *
         * @param {string} code
         * @returns {boolean}
         */
        function isTrustlyMethod(code) {
            return CheckoutConfigLib.getPaymentMethods().some(
                function(method) {
                    return (
                        method.code === code &&
                        method.type === 'PAYMENT_PROVIDER' &&
                        method.specificType === 'INTERNET'
                    );
                }
            );
        }

        /**
         * Check if the customer is a company customer.
         *
         * @returns {boolean}
         */
        function isCompanyCustomer() {
            var address = getAddress();

            return !!(address !== null && address.company);
        }

        return Component.extend({
            defaults: {
                redirectAfterPlaceOrder: true,
                template: 'Resursbank_Mapi/payment/method'
            },

            /**
             * Initialization method.
             */
            initialize: function() {
                var me = this;

                me._super();

                /**
                 * Whether this payment method is from Resurs Bank.
                 *
                 * @type {RbC.Ko.Boolean}
                 */
                me.isResursInternalMethod = ko.observable(
                    isResursInternalMethod(this.getCode())
                );

                /**
                 * Path to the logo of a Resurs Bank payment method.
                 *
                 * @type {string}
                 */
                me.resursBankLogo = require.toUrl(
                    'Resursbank_Mapi/images/logo.png'
                );

                /**
                 * The id number that the customer has entered, if any.
                 *
                 * @type {RbC.Ko.String}
                 */
                me.govId = ko.observable(RESURSBANK_GOV_ID);

                /**
                 * Whether the customer is a company customer.
                 *
                 * @type {boolean}
                 */
                me.isCompanyCustomer = isCompanyCustomer();

                /**
                 * Whether the given id number is invalid.
                 *
                 * @type {boolean}
                 */
                me.invalidGovId = ko.computed(function () {
                    var address = getAddress();

                    return me.govId() !== '' && !CredentialsLib.validate(
                        me.govId(),
                        address.countryId || '',
                        isCompanyCustomer()
                    );
                });

                /**
                 * Whether this payment method is connected to Swish.
                 *
                 * @type {boolean}
                 */
                me.isSwishMethod = isSwishMethod(this.getCode());

                /**
                 * Path to the logo of a Swish payment method.
                 *
                 * @type {string}
                 */
                me.swishLogo = require.toUrl(
                    'Resursbank_Mapi/images/swish.png'
                );

                /**
                 * Whether this payment method is a credit card.
                 *
                 * @type {boolean}
                 */
                me.isCreditCardMethod = isCreditCardMethod(this.getCode());

                /**
                 * Path to the logo of a credit card payment method.
                 *
                 * @type {string}
                 */
                me.creditCardLogo = require.toUrl(
                    'Resursbank_Mapi/images/card.svg'
                );

                /**
                 * Whether this is a Trustly payment method.
                 *
                 * @type {boolean}
                 */
                me.isTrustlyMethod = isTrustlyMethod(this.getCode());

                /**
                 * Path to the logo of a Trustly payment method.
                 *
                 * @type {string}
                 */
                me.trustlyLogo = require.toUrl(
                    'Resursbank_Mapi/images/trustly.svg'
                );

                /**
                 * Whether the payment method has an SSN field. Some methods
                 * require the customer to specify their SSN before checking
                 * out.
                 *
                 * @type {boolean}
                 */
                me.hasSsnField = hasSsnField(me.getCode());

                /**
                 * The availability status of the payment method.
                 *
                 * @type {RbC.Ko.Boolean}
                 */
                me.isAvailable = ko.observable(true);

                /**
                 * Selects the payment method.
                 *
                 * @returns {boolean}
                 */
                me.select = function () {
                    // noinspection JSUnresolvedFunction
                    me.selectPaymentMethod();

                    return true;
                };

                /**
                 * Retrieve configured title for currently selected payment
                 * method.
                 *
                 * @returns {string}
                 */
                me.getTitle = function () {
                    var method = CheckoutConfigLib.getPaymentMethod(
                        me.getCode()
                    );

                    var result = '';

                    if (method) {
                        result = method.title;
                    }

                    return (typeof result === 'string' && result !== '') ?
                        result :
                        me._super();
                };

                /**
                 * Retrieve configured USP for currently selected payment
                 * method.
                 *
                 * @returns {string}
                 */
                me.getUsp = function () {
                    var method = CheckoutConfigLib.getPaymentMethod(
                        me.getCode()
                    );

                    var result = '';

                    if (method) {
                        result = method.usp;
                    }

                    return (typeof result === 'string' && result !== '') ?
                        result :
                        '';
                };

                /**
                 * Retrieve configured Read More link HTML.
                 *
                 * @returns {string}
                 */
                me.getReadMore = function () {
                    var method = CheckoutConfigLib.getPaymentMethod(
                        me.getCode()
                    );

                    var result = '';

                    if (method) {
                        result = method.readMore;
                    }

                    return (typeof result === 'string' && result !== '') ?
                        result :
                        '';
                };

                /**
                 * Retrieve Cost List HTML.
                 *
                 * @returns {string}
                 */
                me.getCostList = function () {
                    var method = CheckoutConfigLib.getPaymentMethod(
                        me.getCode()
                    );

                    var result = '';

                    if (method) {
                        result = method.costList;
                    }

                    return (typeof result === 'string' && result !== '') ?
                        result :
                        '';
                };

                /**
                 * Retrieve Cost List HTML.
                 *
                 * @returns {string}
                 */
                me.getPriceSignageWarning = function () {
                    var method = CheckoutConfigLib.getPaymentMethod(
                        me.getCode()
                    );

                    var result = '';

                    if (method) {
                        result = method.priceSignageWarning;
                    }

                    return (typeof result === 'string' && result !== '') ?
                        result :
                        '';
                };

                /**
                 * Whether all requirements for an order placement has been met.
                 *
                 * @type {RbC.Ko.Boolean}
                 */
                me.canPlaceOrder = ko.computed(function () {
                    return (
                        me.isPlaceOrderActionAllowed()
                    );
                });

                // noinspection JSUnusedLocalSymbols
                /**
                 * Starts the order placement process.
                 *
                 * @param {object} data - Data that KnockoutJS supplies.
                 * @param {object} event
                 */
                me.resursBankPlaceOrder = function (
                    data,
                    event
                ) {
                    if (isCompanyCustomer() && me.invalidGovId()) {
                        me.messageContainer.addErrorMessage({
                            message: 'The organization number is invalid.'
                        });
                        return;
                    }

                    me.placeOrder(data, event);
                };

                /**
                 * Action taken after order has successfully been created.
                 */
                me.afterPlaceOrder = function () {
                    redirectOnSuccessAction.redirectUrl = url.build(
                        'resursbank_mapi/checkout/redirect'
                    );
                };

                /**
                 * Get payment method data.
                 */
                me.getData = function () {
                    // Get original data.
                    var data = Component.prototype.getData.bind(this)();

                    // Append SSN data information.
                    data.additional_data = {
                        'is_company': isCompanyCustomer(),
                        'gov_id': me.govId()
                    };

                    return data;
                };
            }
        });
    }
);
