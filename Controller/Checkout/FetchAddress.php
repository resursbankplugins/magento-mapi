<?php

/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Mapi\Controller\Checkout;

use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\ResultInterface;
use Resursbank\Ecom\Module\Customer\Http\GetAddressController;
use Resursbank\Mapi\Helper\Log;
use Throwable;

/**
 * Fetch customer address from API using supplied SSN and customer type.
 */
class FetchAddress extends GetAddressController implements HttpPostActionInterface
{
    /**
     * @param Log $log
     * @param ResultFactory $resultFactory
     */
    public function __construct(
        private readonly Log $log,
        private readonly ResultFactory $resultFactory,
    ) {
    }

    /**
     * Execute.
     *
     * @throws Throwable
     * @return ResultInterface
     */
    public function execute(): ResultInterface
    {
        try {
            $response = $this->exec(
                data: $this->getRequestData()
            );

            /** @var Json $result */
            $result = $this->resultFactory->create(
                ResultFactory::TYPE_JSON
            );

            $data = json_decode(
                $response,
                true,
                512,
                JSON_THROW_ON_ERROR
            );

            // Format postal code in accordance with Magento standard to avoid
            // validation warnings in checkout form.
            if (isset($data['postalCode'])) {
                // Remove all spaces.
                $data['postalCode'] = preg_replace(
                    '/\s+/',
                    '',
                    $data['postalCode']
                );

                // Inject a space after the first two characters.
                $data['postalCode'] = substr($data['postalCode'], 0, 3) .
                    ' ' .
                    substr($data['postalCode'], 3);
            }

            $result->setData($data);

            return $result;
        } catch (Throwable $e) {
            $this->log->exception($e);
            throw $e;
        }
    }
}
