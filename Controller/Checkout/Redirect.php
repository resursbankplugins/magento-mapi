<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Mapi\Controller\Checkout;

use Exception;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\Controller\Result\Redirect as RedirectResult;
use Magento\Framework\Controller\Result\RedirectFactory;
use Resursbank\Core\Helper\Order;
use Resursbank\Ecom\Lib\Model\PaymentHistory\Entry;
use Resursbank\Ecom\Lib\Model\PaymentHistory\Event;
use Resursbank\Ecom\Lib\Model\PaymentHistory\User;
use Resursbank\Ecom\Module\PaymentHistory\Repository;
use Resursbank\Mapi\Helper\Log;
use Resursbank\Mapi\Helper\Session;
use Resursbank\Core\Helper\Session as CoreSession;

/**
 * Redirect to signing page at Resurs Bank.
 */
class Redirect implements HttpGetActionInterface
{
    /**
     * @param RedirectFactory $redirectFactory
     * @param Session $session
     * @param Log $log
     * @param CoreSession $coreSession
     * @param Order $orderHelper
     * @param CheckoutSession $checkoutSession
     */
    public function __construct(
        private readonly RedirectFactory $redirectFactory,
        private readonly Session $session,
        private readonly Log $log,
        private readonly CoreSession $coreSession,
        private readonly Order $orderHelper,
        private readonly CheckoutSession $checkoutSession
    ) {
    }

    /**
     * @inheritDoc
     *
     * Redirect to signing URL. If there is none, redirect straight to success
     * page.
     *
     * @return RedirectResult
     * @throws Exception
     */
    public function execute(): RedirectResult
    {
        $redirect = $this->redirectFactory->create();

        try {
            $url = (string) $this->session->getPaymentSigningUrl();

            $this->coreSession->useRefererAsFailureRedirectUrl();

            if ($url !== '') {
                // Redirect to Resurs Bank signing page.
                $redirect->setUrl($url);
            } else {
                // Redirect to success page.
                $redirect->setPath('checkout/onepage/success');
            }

            // Log event.
            Repository::write(entry: new Entry(
                paymentId: $this->orderHelper->getPaymentId(
                    order: $this->checkoutSession->getLastRealOrder()
                ),
                event: Event::REDIRECTED_TO_GATEWAY,
                user: User::CUSTOMER
            ));
        } catch (Exception $e) {
            $this->log->exception($e);

            throw $e;
        }

        return $redirect;
    }
}
