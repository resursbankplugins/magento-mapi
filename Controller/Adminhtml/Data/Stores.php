<?php /** @noinspection PhpMultipleClassDeclarationsInspection */
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Mapi\Controller\Adminhtml\Data;

use JsonException;
use Magento\Framework\Controller\Result\JsonFactory;
use ReflectionException;
use Resursbank\Core\Controller\Adminhtml\Data\Stores as CoreStores;
use Resursbank\Core\Helper\Config;
use Resursbank\Core\Helper\Ecom;
use Resursbank\Core\Helper\Log;
use Resursbank\Core\Helper\Scope as ScopeHelper;
use Resursbank\Ecom\Exception\ApiException;
use Resursbank\Ecom\Exception\AuthException;
use Resursbank\Ecom\Exception\CacheException;
use Resursbank\Ecom\Exception\ConfigException;
use Resursbank\Ecom\Exception\CurlException;
use Resursbank\Ecom\Exception\HttpException;
use Resursbank\Ecom\Exception\Validation\EmptyValueException;
use Resursbank\Ecom\Exception\Validation\IllegalTypeException;
use Resursbank\Ecom\Exception\ValidationException;
use Resursbank\Ecom\Lib\Api\GrantType;
use Resursbank\Ecom\Lib\Model\Network\Auth\Jwt;
use Resursbank\Ecom\Lib\Model\Store\GetStoresRequest;
use Resursbank\Ecom\Lib\Model\Store\Store;
use Resursbank\Ecom\Module\Store\Repository as StoreRepository;
use Throwable;

/**
 * Controller for fetching store list.
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Stores extends CoreStores
{
    /**
     * @param Log $log
     * @param JsonFactory $jsonFactory
     * @param Config $config
     * @param ScopeHelper $scope
     * @param Ecom $ecom
     */
    public function __construct(
        Log $log,
        JsonFactory $jsonFactory,
        Config $config,
        ScopeHelper $scope,
        Ecom $ecom
    ) {
        parent::__construct(
            log: $log,
            jsonFactory: $jsonFactory,
            config: $config,
            scope: $scope,
            ecom: $ecom
        );
    }

    /**
     * @inheritDoc
     *
     * @return array
     * @throws EmptyValueException
     * @throws Throwable
     * @throws JsonException
     * @throws ReflectionException
     * @throws ApiException
     * @throws AuthException
     * @throws CacheException
     * @throws ConfigException
     * @throws CurlException
     * @throws HttpException
     * @throws ValidationException
     * @throws IllegalTypeException
     */
    public function getData(): array
    {
        $result = [];

        $this->connectEcom(request: $this->getRequestData());
        $stores = StoreRepository::getStores();

        /** @var Store $store */
        foreach ($stores as $store) {
            $result[$store->id] = $store->name;
        }

        return $result;
    }

    /**
     * Connect to Merchant API.
     *
     * @param GetStoresRequest $request
     * @return void
     * @throws EmptyValueException
     */
    private function connectEcom(GetStoresRequest $request): void
    {
        $this->ecom->connect(
            jwtAuth: new Jwt(
                clientId: $request->clientId,
                clientSecret: $request->clientSecret,
                grantType: GrantType::CREDENTIALS
            ),
            env: $request->environment
        );
    }
}
