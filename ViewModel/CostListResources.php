<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Mapi\ViewModel;

use Magento\Framework\View\Element\Block\ArgumentInterface;
use Resursbank\Ecom\Module\PriceSignage\Widget\CostList;
use Resursbank\Ecom\Module\PriceSignage\Widget\CostListJs;
use Resursbank\Mapi\Helper\Log;
use Throwable;

/**
 * Render Cost List CSS & JS for checkout.
 */
class CostListResources implements ArgumentInterface
{
    /**
     * @param Log $log
     */
    public function __construct(
        private readonly Log $log
    ) {
    }

    /**
     * Get modal CSS.
     *
     * @return string
     */
    public function getCss(): string
    {
        try {
            return CostList::getCss();
        } catch (Throwable $e) {
            $this->log->error($e->getMessage());
        }

        return '';
    }

    /**
     * Get modal JS.
     *
     * @return string
     */
    public function getJs(): string
    {
        try {
            return (new CostListJs(
                containerElDomPath: '#checkout',
            ))->content;
        } catch (Throwable $e) {
            $this->log->error($e->getMessage());
        }

        return '';
    }
}
