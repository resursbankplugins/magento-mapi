<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Mapi\ViewModel;

use Magento\Framework\View\Element\Block\ArgumentInterface;
use Resursbank\Ecom\Module\PaymentMethod\Widget\ReadMore as Widget;
use Resursbank\Ecom\Module\PaymentMethod\Widget\ReadMoreJs;
use Resursbank\Mapi\Helper\Log;
use Throwable;

/**
 * View model for Resurs Bank's widget to render read more modals.
 */
class ReadMore implements ArgumentInterface
{
    /**
     * @param Log $log
     */
    public function __construct(
        private readonly Log $log
    ) {
    }

    /**
     * Get widget CSS.
     *
     * @return string
     */
    public function getCss(): string
    {
        try {
            return Widget::getCss();
        } catch (Throwable $e) {
            $this->log->error($e->getMessage());
        }

        return '';
    }

    /**
     * Get widget JS.
     *
     * @return string
     */
    public function getJs(): string
    {
        try {
            return (new ReadMoreJs(
                containerElDomPath: '#checkout',
                autoInitJs: true
            ))->content;
        } catch (Throwable $e) {
            $this->log->error($e->getMessage());
        }

        return '';
    }
}
