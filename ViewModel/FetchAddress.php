<?php
/**
 * Copyright Â© Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Mapi\ViewModel;

use Magento\Framework\App\ProductMetadataInterface;
use Magento\Framework\Data\Form\FormKey;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Resursbank\Ecom\Module\Customer\Widget\GetAddress;
use Resursbank\Mapi\Helper\Log;
use Throwable;

/**
 * View model for Resurs Bank's widget to fetch a customer's address based on
 * provided SSN or organisation number.
 */
class FetchAddress implements ArgumentInterface
{
    /**
     * Constructor.
     *
     * @param FormKey $formKey
     * @param Log $log
     * @param UrlInterface $url
     * @param ProductMetadataInterface $productMetadata
     */
    public function __construct(
        private readonly FormKey $formKey,
        private readonly Log $log,
        private readonly UrlInterface $url,
        private readonly ProductMetadataInterface $productMetadata
    ) {
    }

    /**
     * Get widget instance.
     *
     * @return GetAddress|null
     */
    public function getWidget(): ?GetAddress
    {
        try {
            return new GetAddress(
                url: $this->url->getUrl(
                    routePath: 'resursbank_mapi/checkout/fetchAddress',
                    routeParams: ['form_key' => $this->formKey->getFormKey()]
                )
            );
        } catch (Throwable $e) {
            $this->log->error($e->getMessage());
            return null;
        }
    }

    /**
     * Check if Magento version is less than 2.4.7
     *
     * We use this to render JS / CSS tags differently on the checkout page,
     * because the requirements are different for 2.4.7+
     *
     * @return bool
     */
    public function isOldMagento(): bool
    {
        return version_compare($this->productMetadata->getVersion(), '2.4.7', '<');
    }
}
