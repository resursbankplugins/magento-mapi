# Resurs Bank - Magento 2 module - MAPI API integration

## Description

MAPI API implementation for Magento 2 module.

---

## Prerequisites

* [Magento 2](https://devdocs.magento.com/guides/v2.4/install-gde/bk-install-guide.html) [Supports Magento 2.4.1+]
